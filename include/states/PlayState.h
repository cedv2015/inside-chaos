#ifndef PlayState_H
#define PlayState_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include <CEGUI.h>
#include <RendererModules/Ogre/Renderer.h>
#include "PhysicsManager.h"
#include "ScenarioManager.h"
#include "Player.h"
#include "CameraManager.h"
#include "GameState.h"
#include "PauseState.h"
#include "GameOverState.h"
#include "EntitiesManager.h"
#include "ParticlesManager.h"

class PlayState : public Ogre::Singleton<PlayState>, public GameState {
    public:
        PlayState();
        static PlayState& getSingleton ();
        static PlayState* getSingletonPtr ();
        void enter ();
        void exit ();
        void pause ();
        void resume ();
        void hud ();
        void blood ();
        void updateHUD();
        void hideHud();
        void updatePoints(int p);
        int getPoints();

        void bloodShow();
        void hurtPlayer();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);
        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

        bool frameStarted (const Ogre::FrameEvent &e);
        bool frameEnded (const Ogre::FrameEvent &e);

    private:
        Ogre::Root *_root;
        Ogre::SceneManager *_sceneMgr;
        Ogre::Camera *_camera;
        Ogre::Viewport *_viewport;
        OIS::Keyboard *_keyboard;
        OIS::Mouse *_mouse;
        bool _exit, _bloodVisible;
        PhysicsManager *_physicsMgr;
        EntitiesManager *_entitiesMgr;
        ScenarioManager *_scenarioMgr;
        CameraManager *_cameraMgr;
        Player *_player;

        CEGUI::Window * _numGrenades, *_numEnemies, *_blood, *_hud;
        ParticlesManager *_particlesMgr;
};

#endif
