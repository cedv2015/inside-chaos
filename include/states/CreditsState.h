#ifndef CREDITSSTATE_H
#define CREDITSSTATE_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include <CEGUI.h>
#include <RendererModules/Ogre/Renderer.h>
#include "PlayState.h"
#include "MenuState.h"

class CreditsState : public Ogre::Singleton<CreditsState>, public GameState {
    public:
        CreditsState ();
        static CreditsState& getSingleton ();
        static CreditsState* getSingletonPtr ();
        void enter ();
        void exit ();
        void pause ();
        void resume ();
        void wallpaper();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);
        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        CEGUI::MouseButton convertMouseButton(const OIS::MouseButtonID id);

        bool frameStarted (const Ogre::FrameEvent &e);
        bool frameEnded (const Ogre::FrameEvent &e);

        void creditsShow();
        bool hide(const CEGUI::EventArgs &e);

    private:
  	    Ogre::Root* _root;
  	    Ogre::SceneManager* _sceneMgr;
  	    Ogre::Viewport* _viewport;
  	    Ogre::Camera* _camera;

  	    OIS::Keyboard *_keyboard;
        OIS::Mouse *_mouse;
        bool _exit;
        CEGUI::Window* _credits;
};

#endif
