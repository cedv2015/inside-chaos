#ifndef GameOverState_H
#define GameOverState_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include <CEGUI.h>
#include <RendererModules/Ogre/Renderer.h>
#include "PhysicsManager.h"
#include "ScenarioManager.h"
#include "GameState.h"
#include "PlayState.h"
#include "EntitiesManager.h"
#include "RankingManager.h"

class GameOverState : public Ogre::Singleton<GameOverState>, public GameState {
    public:
        GameOverState();
        static GameOverState& getSingleton ();
        static GameOverState* getSingletonPtr ();
        void enter ();
        void exit ();
        void pause ();
        void resume ();

        void setScore(int s){_score=s;};
        int getScore(){return _score;};
        void addRanking();
        void gameOver();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);
        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        CEGUI::MouseButton convertMouseButton(const OIS::MouseButtonID id);

        bool frameStarted (const Ogre::FrameEvent &e);
        bool frameEnded (const Ogre::FrameEvent &e);
        bool hide(const CEGUI::EventArgs &e);
        bool accept(const CEGUI::EventArgs &e);
        bool close(const CEGUI::EventArgs &e);

    private:
        Ogre::Root *_root;
        Ogre::SceneManager *_sceneMgr;
        Ogre::Camera *_camera;
        OIS::Keyboard *_keyboard;
        OIS::Mouse *_mouse;
        bool _exit;
        PhysicsManager *_physicsMgr;
        EntitiesManager *_entitiesMgr;
        RankingManager *_rankingMgr;
        int _score;
        CEGUI::Window* _ranking, *_exitButton;
};

#endif
