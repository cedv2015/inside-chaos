#ifndef AnimationBlender_H
#define AnimationBlender_H

#include <Ogre.h>

class AnimationBlender {
    public:
        enum BlendingTransition {
            Switch,  // Parar fuente y reproduce destino
            Blend    // Cross fade (Mezclado suave)
        };
   
    private:
        Ogre::Entity *mEntity;           // Entidad sobre la que mezclamos
        Ogre::AnimationState *mSource;   // Animacion inicio de la mezcla
        Ogre::AnimationState *mTarget;   // Animacion destino
        BlendingTransition mTransition;  // Tipo de transicion
        bool mLoop;                      // Animacion en bucle?
   
    public: 
        Ogre::Real mTimeleft;    // Tiempo restante de la animacion (segundos)
        Ogre::Real mDuration;    // Tiempo invertido en el mezclado (segundos)
        bool mComplete;          // Ha finalizado la animacion?
       
        AnimationBlender(Ogre::Entity *);
        void blend(const Ogre::String &animation, BlendingTransition transition, 
                   Ogre::Real duration, bool l=true);
        void addTime(Ogre::Real);
        Ogre::Real getProgress() { return mTimeleft/mDuration; }
        Ogre::AnimationState *getSource() { return mSource; }
        Ogre::AnimationState *getTarget() { return mTarget; }
        bool getLoop() { return mLoop; }
};

#endif
