#ifndef CONSTANTS_H
#define CONSTANTS_H

namespace Constants {
    const unsigned int NUMBER_ELEMENTS_PER_SCENARIO = 3;
    const float WIDTH_OF_MODULES = 6;
    const float PLAYER_SPEED = 100;

    enum MyCollisionFilterGroups {
        PLAYER = 1,
        PLAYERBULLET = 2,
        ENEMYBULLET = 4,
        MIDDLEPLANE = 8,
        EXTERNALPLANE = 16,
        ENEMY = 32,
        LATERALPLANE = 64,
        FLOOR = 128,
        ROLLERBULLET = 256,
        TURRETBULLET = 512,
        BORDERLANDBULLET = 1024,
        GRENADE = 2048,
        GRENADEEXPLOSION = 4096
    };

    const short player_collides_with = ENEMYBULLET | MIDDLEPLANE | EXTERNALPLANE | LATERALPLANE | FLOOR | ROLLERBULLET | TURRETBULLET;
    const short playerBullet_collides_with = EXTERNALPLANE | ENEMY | LATERALPLANE | FLOOR | ROLLERBULLET | TURRETBULLET;
    const short enemyBullet_collides_with = EXTERNALPLANE | PLAYER | LATERALPLANE | FLOOR;
    const short rollerBullet_collides_with = EXTERNALPLANE | PLAYER | FLOOR | PLAYERBULLET | GRENADE | GRENADEEXPLOSION;
    const short externalPlane_collides_with = PLAYERBULLET | ENEMYBULLET | PLAYER | ENEMY | FLOOR | ROLLERBULLET | TURRETBULLET | BORDERLANDBULLET | GRENADE;
    const short middlePlane_collides_with = PLAYER | FLOOR;
    const short enemy_collides_with = PLAYERBULLET | EXTERNALPLANE | FLOOR | GRENADE | GRENADEEXPLOSION;
    const short lateralPlane_collides_with = PLAYERBULLET | ENEMYBULLET | PLAYER | FLOOR | GRENADE;
    const short floor_collides_with = PLAYER | PLAYERBULLET | ENEMYBULLET | MIDDLEPLANE | EXTERNALPLANE | ENEMY | LATERALPLANE | ROLLERBULLET | TURRETBULLET | GRENADE;
    const short grenade_collides_with = EXTERNALPLANE | ENEMY | LATERALPLANE | FLOOR | ROLLERBULLET;
    const short grenadeExplosion_collides_with = ROLLERBULLET | ENEMY;

    const unsigned int TURRETBULLETVELOCITY = 13;
    const unsigned int BORDERLANDBULLETVELOCITY = 13;

    const unsigned int LIFEENEMY = 100;
    const unsigned int REDUCELIFEENEMY = 20;
    const unsigned int LIFEPLAYER = 200;
    const unsigned int REDUCELIFEPLAYER = 10;

     const unsigned int TIMESHOOTBORDERLAND = 3;
     const unsigned int TIMESHOOTTURRET = 2;
     const unsigned int TIMESHOOTROLLER = 2;

     const unsigned int GRENADETIME = 3;

      const unsigned int POINTSBORDERLAND = 20;
      const unsigned int POINTSTURRET = 30;
      const unsigned int POINTSROLLER = 10;

}

namespace Particles {
    enum MyParticles {
        GRENADEEXPLOSION,
        SPARKS,
        LASER,
        ENEMYEXPLOSION,
        ROLLEREXPLOSION,
        PLAYERBULLET
    };
}

#endif
