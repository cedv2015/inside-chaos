#ifndef ENEMY_H
#define ENEMY_H

#include "Player.h"
#include "SoundFXManager.h"

class EntitiesManager;

class Enemy : public DynamicEntity {
public:
    Enemy(Ogre::SceneNode *node, Ogre::Entity *entity, OgreBulletDynamics::RigidBody *body, OgreBulletCollisions::CollisionShape *shape, Player *player, int type, float iddleTime) : DynamicEntity(node, entity, body, shape, type), _life(Constants::LIFEENEMY), _player(player), _iddleTime(iddleTime), _entitiesMgr(nullptr) {};
    virtual ~Enemy() {};

    virtual void shoot(float delta) = 0;
    int getLife(){return _life;};
    void reduceLife(int x){_life-=x;};

protected:
    int _life;
    Player *_player;
    float _iddleTime;
    EntitiesManager *_entitiesMgr;
    SoundFXPtr _sound_shoot;
    SoundFXPtr _sound_die;
};

#endif
