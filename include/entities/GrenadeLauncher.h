#ifndef GRENADELAUNCHER_H
#define GRENADELAUNCHER_H

#include "Weapon.h"

class GrenadeLauncher : public Weapon {
public:
    GrenadeLauncher();
    ~GrenadeLauncher() {};

    void shoot (bool last_clicked) override;

    void update (float delta) override;

    void swapVisibility() override;

    int getGrenadesLeft(){return _grenades_left;};
private:
    int _grenades_left;
};

#endif
