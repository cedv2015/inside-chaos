#ifndef DOOR_H
#define DOOR_H

#include "DynamicEntity.h"
#include "AnimationBlender.h"
#include "SoundFX.h"

class Door : public DynamicEntity{
public:
    Door(int position);
    ~Door() {
        delete _animation;
    };

    void update (float delta) override;

    void moveForward();

    void open();

    void close();

private:
    int _position;
    AnimationBlender *_animation;
    SoundFXPtr _door_open;
    SoundFXPtr _door_close;
};

#endif
