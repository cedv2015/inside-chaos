#ifndef BORDERLAND_H
#define BORDERLAND_H

#include "Enemy.h"
#include "AnimationBlender.h"

class Borderland : public Enemy{
public:

    enum States {
        Q0,  // Movimiento lateral hacia la derecha
        Q1,  // Movimiento lateral hacia la izquierda
        Q2,  // Movimiento hacia delante y hacia atras (como no hay cambio en la animacion se ha considerado del misto estado.)
        Q3,  // Giro hacia la derecha
        Q4,  // Giro a la izquierda
        Q5,  // Giro 180 dir: derecha-izquiera
        Q6,  // Giro 180 dir: izquierda-derecha
    };
    Borderland(Player *player, bool goingRight, float zPosition, int id, float iddleTime);
    ~Borderland() {delete _animation;};

    void update(float delta) override;

protected:
    bool _goingRight, _goForward, _lateral, _turn, _cerrojo;
    int _directionChangesLeft, _directionChangesBack;
    float _timeSinceLastShoot, _timeForward, _zInitial, _timeAnimation;
    int _id, _continue;
    AnimationBlender *_animation;
    Ogre::Skeleton *_skeleton;
    States _state;


    void shoot (float delta) override;

    void run (float delta);

    void dissapear ();

    void onCollision (Entity *entity) override;

    void animation(float delta);

    static int _bulletsBorderlandId;
};

#endif
