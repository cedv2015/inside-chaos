#ifndef TURRETBULLET_H
#define TURRETBULLET_H

#include "Bullet.h"
#include "ParticlesManager.h"

class TurretBullet : public Bullet {
public:
    TurretBullet(Ogre::Vector3 initial_position, int id, Ogre::Vector3 position_player);
    ~TurretBullet() {
        _particle->finish();
    };

    void update (float delta);
    void onCollision(Entity *entity) override;
private:
    MyParticleSystem *_particle;
};

#endif
