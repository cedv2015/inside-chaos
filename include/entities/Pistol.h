#ifndef PISTOL_H
#define PISTOL_H

#include "Weapon.h"
#include "AnimationBlender.h"

class Pistol : public Weapon {
public:
    Pistol(Ogre::Vector3 player_position);
    ~Pistol(){};

    void shoot(bool last_clicked) override;

    void update (float delta) override;

    void swapVisibility () override;

protected:

    Ogre::Entity *_entity;
    Ogre::SceneNode *_node;
    AnimationBlender *_animation;
};

#endif
