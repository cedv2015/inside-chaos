#ifndef FLOOR_H
#define FLOOR_H

#include "StaticEntity.h"

class Floor : public StaticEntity{
public:
    Floor(Ogre::SceneNode *node, Ogre::Entity *entity, OgreBulletDynamics::RigidBody *body, OgreBulletCollisions::CollisionShape *shape) : StaticEntity(node, entity, body, shape, Type::FLOOR) {
        _entity->setCastShadows(false);
        auto bullet_body = _body->getBulletRigidBody();
        bullet_body->setUserPointer(this);
    };
    ~Floor() {};

    void onCollision (Entity *entity) override {};

private:
};

#endif
