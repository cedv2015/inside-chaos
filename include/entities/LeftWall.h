#ifndef LEFTWALL_H
#define LEFTWALL_H

#include "StaticEntity.h"

class LeftWall : public StaticEntity {
public:
    LeftWall(Ogre::SceneNode *node, Ogre::Entity *entity, OgreBulletDynamics::RigidBody *body, OgreBulletCollisions::CollisionShape *shape) : StaticEntity(node, entity, body, shape, Type::LEFTWALL) {
        _entity->setCastShadows(false);
        auto bullet_body = _body->getBulletRigidBody();
        bullet_body->setUserPointer(this);
    };
    ~LeftWall() {};

    void onCollision (Entity *entity) override {};
private:
    /* data */
};

#endif
