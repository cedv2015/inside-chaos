#ifndef DYNAMICENTITY_H
#define DYNAMICENTITY_H

#include "Entity.h"

class DynamicEntity : public Entity {
public:
    DynamicEntity(Ogre::SceneNode *node, Ogre::Entity *entity, OgreBulletDynamics::RigidBody *body, OgreBulletCollisions::CollisionShape *shape, int type) : Entity(node, entity, body, shape, type) {};
    virtual ~DynamicEntity() {
    };

    virtual void update(float delta) = 0;

};

#endif
