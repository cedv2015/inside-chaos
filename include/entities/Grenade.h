#ifndef GRENADE_H
#define GRENADE_H

#include "DynamicEntity.h"
#include "SoundFXManager.h"

class Grenade : public DynamicEntity{
public:
    Grenade();
    ~Grenade() {};

    void update (float delta) override;
    
    void explode ();
private:
    float _time_alive;
    bool _exploded;
    static int _id;
    SoundFXPtr _sound_explosion;
    Ogre::SceneManager *_sceneMgr;
};

#endif
