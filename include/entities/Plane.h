#ifndef PLANE_H
#define PLANE_H

#include "DynamicEntity.h"

class Plane : public DynamicEntity {
public:
    Plane(Ogre::SceneNode *node, Ogre::Entity *entity, OgreBulletDynamics::RigidBody *body, OgreBulletCollisions::CollisionShape *shape) : DynamicEntity(node, entity, body, shape, Type::PLANE) {
        _entity->setCastShadows(false);
        auto bullet_body = _body->getBulletRigidBody();
        bullet_body->setUserPointer(this);
        bullet_body->setAngularFactor(btVector3(0, 0, 0));
        bullet_body->setLinearFactor(btVector3(0, 0, 0));
    }
    ~Plane() {};

    void onCollision (Entity *entity) override;
    void update (float delta) override;
    void moveForward(int i);

private:
    bool _hide;
};

#endif
