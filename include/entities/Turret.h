#ifndef TURRET_H
#define TURRET_H

#include "Enemy.h"

class Turret : public Enemy{
public:
    Turret(Player *player, float zPosition, int id, float iddleTime, int position);
    ~Turret() {};

    void update(float delta) override;

protected:
    float _timeSinceLastShoot, _timeLive;
    int _id;

    void shoot (float delta) override;

    void run ();

    void dissapear ();

    void onCollision (Entity *entity) override;

    static int _turretBulletsId;

    Ogre::Skeleton *_skeleton;
};

#endif
