#ifndef BORDERLANDBULLET_H
#define BORDERLANDBULLET_H

#include "Bullet.h"
#include "ParticlesManager.h"

class BorderlandBullet : public Bullet {
public:
    BorderlandBullet(Ogre::Vector3 initial_position, int id, Ogre::Vector3 position_player);
    ~BorderlandBullet() {
        _particle->finish();
    };

    void update (float delta);
    void onCollision(Entity *entity) override;
private:
    MyParticleSystem *_particle;
};

#endif
