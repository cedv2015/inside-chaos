#ifndef PLAYERBULLET_H
#define PLAYERBULLET_H

#include "Bullet.h"
#include "ParticlesManager.h"

class PlayerBullet : public Bullet {
public:
    PlayerBullet(int id);
    ~PlayerBullet(){
        _particle->finish();
    };
    void update (float delta) override;
    void onCollision(Entity *entity) override;

private:
    float _time;
    Ogre::SceneManager *_sceneMgr;
    MyParticleSystem *_particle;
};

#endif
