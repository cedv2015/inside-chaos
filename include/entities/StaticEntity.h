#ifndef STATICENTITY_H
#define STATICENTITY_H

#include "Entity.h"

class StaticEntity : public Entity{
public:
    StaticEntity(Ogre::SceneNode *node, Ogre::Entity *entity, OgreBulletDynamics::RigidBody *body, OgreBulletCollisions::CollisionShape *shape, int type) : Entity(node, entity, body, shape, type) {};
    virtual ~StaticEntity() {};

protected:
};

#endif
