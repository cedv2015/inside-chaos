#ifndef ROLLER_H
#define ROLLER_H

#include "Enemy.h"
#include "AnimationBlender.h"

class Roller : public Enemy {
public:
    Roller(Player *player, bool goingRight, float zPosition, int id, float iddleTime);
    ~Roller() {
        delete _animation;
    };

    void update(float delta) override;

protected:
    bool _goingRight;
    int _directionChangesLeft;
    float _timeSinceLastShoot;
    int _id;
    AnimationBlender *_animation;
    Ogre::Skeleton *_skeleton;

    void shoot (float delta) override;

    void run ();

    void dissapear ();

    void onCollision (Entity *entity) override;

    static int _bulletsId;
    const float _speed = 2.0;
};


#endif
