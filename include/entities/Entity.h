#ifndef ENTITY_H
#define ENTITY_H

#include <Ogre.h>
#include <OgreBulletDynamicsRigidBody.h>
#include <Shapes/OgreBulletCollisionsCapsuleShape.h>

class Entity {
public:
    Entity(Ogre::SceneNode *node, Ogre::Entity *entity, OgreBulletDynamics::RigidBody *body, OgreBulletCollisions::CollisionShape *shape, int type) : _node(node), _entity(entity), _body(body), _shape(shape), _type(type), _remove(false) {};
    virtual ~Entity() {
        if (_body) {
            delete _body;
        }
        if (_shape) {
            delete _shape;
        }
        if (_node && _entity) {
            _node->getParent()->removeChild(_node);
            _node->detachAllObjects();
            Ogre::SceneManager *sceneMgr = Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager");
            sceneMgr->destroySceneNode(_node);
            sceneMgr->destroyEntity(_entity);
        }
    };

    virtual void onCollision(Entity *entity) {};
    bool isFinished () { return _remove; };
    OgreBulletDynamics::RigidBody* getRigidBody () { return _body; };
    Ogre::Entity* getEntity () { return _entity; };
    int getType () { return _type; };
protected:
    Ogre::SceneNode *_node;
    Ogre::Entity *_entity;

    OgreBulletDynamics::RigidBody *_body;
    OgreBulletCollisions::CollisionShape *_shape;


    int _type;

    bool _remove;

};

enum Type {
    PLAYER,
    PLAYERBULLET,
    ENEMY,
    ENEMYBULLET,
    ROLLERBULLET,
    TURRETBULLET,
    BORDERLANDBULLET,
    PLANE,
    LEFTWALL,
    RIGHTWALL,
    FLOOR,
    GRENADE,
    GRENADEEXPLOSION,
    DOOR
};

#endif
