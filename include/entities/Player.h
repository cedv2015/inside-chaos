#ifndef PLAYER_H
#define PLAYER_H

#include "DynamicEntity.h"
#include "Machinegun.h"
#include "Pistol.h"
#include "GrenadeLauncher.h"
#include "SoundFXManager.h"

class Player : public DynamicEntity{
public:
    Player();
    ~Player() {};
    void update (float delta) override;
    void pressLeft () { _left = true; };
    void pressRight () { _right = true; };
    void releaseLeft () { _left = false; };
    void releaseRight () { _right = false; };
    void pressUp () { _up = true; };
    void releaseUp () { _up = false; };
    void pressForward () { _forward = true; };
    void releaseForward () { _forward = false; };
    void pressBack () { _back = true; };
    void releaseBack () { _back = false; };
    void releaseGrenade () { _throwGrenade = true; }
    void onCollision(Entity *entity) override;
    void jump ();
    float getZ();
    void pressButtonLeft () { _bleft = true; };
    void releaseButtonLeft () { _bleft = false; };
    void addRotation (float yaw) { _rotation += yaw; };
    Ogre::Vector3 getPosition();
    int getLife(){return _life;};
    void reduceLife(int x){_life-=x;};
    bool isDead(){return _dead;};
    void updateWeapon() { _weapon->update(0);};
    void swapWeapon();
    int getGrenadesLeft();
    void setHurt(bool h){_hurt = h;};
    bool getHurt(){ return _hurt;};
    void hideWeapon();
    void updatePoints(int p){_points+=p;};
    int getPoints(){return _points;};

private:
    int _life, _points;
    bool _left, _right, _up, _onGround, _onLeftWall, _onRightWall, _lastFrameUp,
         _forward, _back, _bleft, _lastclicked, _onBackWall, _throwGrenade, _dead, _hurt;
    Weapon *_pistol;
    Weapon *_machinegun;
    Weapon *_weapon;
    Weapon *_grenadeLauncher;
    float _rotation;
    void throwGrenade ();
    //Sound stuff
    SoundFXPtr _sound_jump;
    SoundFXPtr _sound_grenade_launch;
    int _actual_weapon;

};

#endif
