#ifndef WEAPON_H
#define WEAPON_H

#include "PlayerBullet.h"
#include "SoundFX.h"
#include <stdlib.h>
class EntitiesManager;

class Weapon{
public:
    Weapon(): _entitiesMgr(nullptr) {
    };
    ~Weapon() {};

    virtual void shoot(bool last_clicked) = 0;

    virtual void update (float delta) = 0;
    
    virtual void swapVisibility() = 0;
protected:
    static int _shots;
    EntitiesManager *_entitiesMgr;
    float _reloadTime;
    Ogre::Camera *_camera;
    SoundFXPtr _sound_shoot;
};

#endif
