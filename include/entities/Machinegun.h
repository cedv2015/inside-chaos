#ifndef MACHINEGUN_H
#define MACHINEGUN_H

#include "Weapon.h"
#include "AnimationBlender.h"

class Machinegun : public Weapon {
public:
    Machinegun(Ogre::Vector3 player_position);
    ~Machinegun(){};

    void shoot (bool last_clicked) override;

    void update (float delta) override;

    void swapVisibility() override;

protected:

    float _last_shot;
    Ogre::Entity *_entity;
    Ogre::SceneNode *_node;
    AnimationBlender *_animation;
};

#endif
