#ifndef RIGHTWALL_H
#define RIGHTWALL_H

#include "StaticEntity.h"

class RightWall : public StaticEntity {
public:
    RightWall(Ogre::SceneNode *node, Ogre::Entity *entity, OgreBulletDynamics::RigidBody *body, OgreBulletCollisions::CollisionShape *shape) : StaticEntity(node, entity, body, shape, Type::RIGHTWALL) {
        _entity->setCastShadows(false);
        auto bullet_body = _body->getBulletRigidBody();
        bullet_body->setUserPointer(this);
    }
    ~RightWall() {};

    void onCollision (Entity *entity) override {};

private:
    /* data */
};

#endif
