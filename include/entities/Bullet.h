#ifndef BULLET_H
#define BULLET_H

#include "DynamicEntity.h"
#include "Constants.h"
#include "PhysicsManager.h"

class Bullet : public DynamicEntity {
public:
    Bullet(Ogre::SceneNode *node, Ogre::Entity *entity, OgreBulletDynamics::RigidBody *body, OgreBulletCollisions::CollisionShape *shape, int type, int id): DynamicEntity(node, entity, body, shape, type), _id(id) {};
    virtual ~Bullet() {};

    virtual void update (float delta) = 0;
    virtual void onCollision(Entity *entity) override = 0;
    int getId(){return _id;};
private:
    int _id;
};

#endif
