#ifndef ROLLERBULLET_H
#define ROLLERBULLET_H

#include "Bullet.h"

class RollerBullet : public Bullet {
public:
    RollerBullet(Ogre::Vector3 initial_position, int id);
    ~RollerBullet() {};

    void update (float delta);
    void onCollision(Entity *entity) override;
private:
    /* data */
};

#endif
