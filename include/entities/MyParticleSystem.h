#ifndef MYPARTICLESYSTEM_H
#define MYPARTICLESYSTEM_H

#include <Ogre.h>
#include <OgreParticleSystem.h>

class MyParticleSystem {
public:
    MyParticleSystem(Ogre::Vector3 position, std::string name, int id);
    ~MyParticleSystem();

    void update (float delta);

    void setPosition (Ogre::Vector3 position);

    void finish ();
    
    bool isFinished () { return _is_finished; };
private:
    Ogre::ParticleSystem *_particle;
    Ogre::SceneNode *_parent;
    bool _is_finished;
    float _time_to_finish;
};

#endif
