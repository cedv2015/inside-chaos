#ifndef ENTITIESMANAGER_H
#define ENTITIESMANAGER_H

#include <Ogre.h>
#include <vector>
#include "Player.h"
#include "Roller.h"
#include "Turret.h"
#include "Borderland.h"
#include "StaticEntity.h"
#include "ScenarioManager.h"
#include "EnemyLoader.h"

class EntitiesManager : public Ogre::Singleton<EntitiesManager> {
public:
    EntitiesManager();
    ~EntitiesManager();

    static EntitiesManager& getSingleton();
    static EntitiesManager* getSingletonPtr();

    void update (float delta);

    Player* createPlayer ();

    void addStaticEntity (StaticEntity *entity);
    void addDynamicEntity (DynamicEntity *entity);

    Player* getPlayer(){return _player;};

    void removeEntities();

private:
    std::vector<DynamicEntity*> _entities;
    std::vector<StaticEntity*> _staticEntities;

    ScenarioManager *_scenarioMgr;

    int _number_enemies;

    Player *_player;

    bool _update_enemies;

    EnemyLoader *_enemyLdr;

    void createEnemies ();
};

#endif
