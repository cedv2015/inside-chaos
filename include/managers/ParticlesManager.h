#ifndef PARTICLESMANAGER_H
#define PARTICLESMANAGER_H

#include "MyParticleSystem.h"

class ParticlesManager : public Ogre::Singleton<ParticlesManager> {
public:
    ParticlesManager();
    ~ParticlesManager();

    void update (float delta);

    MyParticleSystem* createParticleSystem (Ogre::Vector3 position, int type);

    void destroyAllParticles ();

    static ParticlesManager& getSingleton ();
    static ParticlesManager* getSingletonPtr ();

private:
    std::vector<MyParticleSystem*> _particles;
    int _id_counter;
};

#endif
