#ifndef ENEMYLOADER_H
#define ENEMYLOADER_H

#include <Ogre.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include "Player.h"
#include "DynamicEntity.h"

class EnemyLoader : public Ogre::Singleton<EnemyLoader> {
public:
    EnemyLoader () {};
    ~EnemyLoader () {};

    static EnemyLoader& getSingleton ();
    static EnemyLoader* getSingletonPtr ();

    std::vector<DynamicEntity*> generateEnemies (float zposition, Player *player);

private:
    std::vector<DynamicEntity*> loadBorderlands (float zposition, Player *player);
    std::vector<DynamicEntity*> loadRollers (float zposition, Player *player);
    std::vector<DynamicEntity*> loadTurrets (float zposition, Player *player);

    const static int BORDERLAND_FILE_NUMBER = 1;
    const static int ROLLER_FILE_NUMBER = 1;
    const static char DELIM = ':';
};

#endif
