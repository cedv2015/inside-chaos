#ifndef CAMERAMANAGER_H
#define CAMERAMANAGER_H

#include <Ogre.h>
#include <OgreBulletDynamicsRigidBody.h>

class CameraManager: public Ogre::Singleton<CameraManager> {
public:
    CameraManager(Ogre::Camera *camera) : _camera(camera), _player(nullptr), _eyehole_nod(nullptr), _eyehole_ent(nullptr) {};
    static CameraManager& getSingleton ();
    static CameraManager* getSingletonPtr ();
    ~CameraManager() {};

    void setPlayer(OgreBulletDynamics::RigidBody *player) { 
        _player = player;
        Ogre::SceneManager *sceneMgr = Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager");
        _eyehole_nod = sceneMgr->createSceneNode("EyeholeNode");
        _eyehole_ent = sceneMgr->createEntity("Eyehole", "mirilla.mesh");
        sceneMgr->getRootSceneNode()->addChild(_eyehole_nod);
        _eyehole_nod->attachObject(_eyehole_ent);
        _eyehole_nod->scale(Ogre::Vector3(0.07, 0.07, 0.07));
    };

    void update (float delta);

    void clear ();

private:
    Ogre::Camera *_camera;
    OgreBulletDynamics::RigidBody *_player;
    Ogre::SceneNode *_eyehole_nod;
    Ogre::Entity *_eyehole_ent;
};

#endif
