#ifndef PHYSICSMANAGER_H
#define PHYSICSMANAGER_H

#include <Ogre.h>

#include <OgreBulletDynamicsRigidBody.h>
#include <Shapes/OgreBulletCollisionsStaticPlaneShape.h>

class PhysicsManager : public Ogre::Singleton<PhysicsManager>  {
    public:
        PhysicsManager();
        ~PhysicsManager();

        static PhysicsManager& getSingleton();
        static PhysicsManager* getSingletonPtr();

        void update(float delta);
        OgreBulletDynamics::DynamicsWorld * getWorld() { return _world; };

        void addRigidBody(OgreBulletDynamics::RigidBody *body);
        void addCollisionShape(OgreBulletCollisions::CollisionShape *shape);

        void init();
        void switchDebug ();

    private:
        Ogre::Root *_root;
        Ogre::SceneManager *_sceneMgr;
        OgreBulletDynamics::DynamicsWorld *_world;
        OgreBulletCollisions::DebugDrawer *_debugDrawer;
        std::deque <OgreBulletDynamics::RigidBody *> _bodies;
        std::deque <OgreBulletCollisions::CollisionShape *> _shapes;
        std::deque <OgreBulletDynamics::RigidBody *> _bodiesScenario;
        std::deque <OgreBulletCollisions::CollisionShape *> _shapesScenario;

        void removeObjects();
        void removeScenario();
        // Collisions
        void checkCollisions();
        bool _debug;
};

#endif
