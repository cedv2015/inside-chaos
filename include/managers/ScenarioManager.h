#ifndef ScenarioManager_H
#define ScenarioManager_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include <OgreSingleton.h>
#include <vector>

#include <stdlib.h>

#include "PhysicsManager.h"
#include "Plane.h"
#include "Door.h"

#include "Constants.h"


class ScenarioManager : public Ogre::Singleton<ScenarioManager> {
    public:
        ScenarioManager ();
        ~ScenarioManager () {};

        static ScenarioManager& getSingleton ();
        static ScenarioManager* getSingletonPtr ();

        void getScene ();
        void newStage();
        void generateScenarioRandom();
        void generateInitialNodes();
        void createPlanes();
        void createDoors();

        void removeLastScenario();

        void removeAllElements();

        int getNumberOfStages() { return _number_of_stages; };
        float getThreshold() { return -100 + (_number_of_stages) * 3 * Constants::WIDTH_OF_MODULES + 3; };

    private:
        Ogre::SceneManager* _sceneManager;
        Ogre::SceneNode* _stage;
        int _number_of_stages;

        std::vector<Ogre::SceneNode*> _nodesScenario;
        std::vector<Ogre::Entity*> _entitiesScenario;

        // The number of elements that belong to a scenario, for now it's just
        // the 3 modules, but in the future, doors can be added or similar
        Plane *_pA, *_pB, *_pC;
        bool _openDoor;
        Ogre::Light *_light;
        void openDoor();
        std::vector<Door*> _doors;
        int _actual_door;
};

#endif
