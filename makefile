EXEC := Inside_Chaos

DIRSRC := src/
DIROBJ := obj/
DIRHEA := include/

DIRSRCENTITIES:= $(DIRSRC)entities/
DIRSRCSTATES:= $(DIRSRC)states/
DIRSRCMANAGERS:= $(DIRSRC)managers/
DIRSRCAUDIO:= $(DIRSRC)audio/
DIRHEAENTITIES:= $(DIRHEA)entities/
DIRHEASTATES:= $(DIRHEA)states/
DIRHEAMANAGERS:= $(DIRHEA)managers/
DIRHEAAUDIO:= $(DIRHEA)audio/

CXX := g++

# Flags de compilación -----------------------------------------------
CXXFLAGS := -I $(DIRHEA) -I $(DIRHEAENTITIES) -I $(DIRHEASTATES) -I $(DIRHEAMANAGERS) -I $(DIRHEAAUDIO) -Wall -std=c++11 -I/usr/local/include/cegui-0/CEGUI -I/usr/local/include/cegui-0 `pkg-config --cflags OGRE OGRE-Overlay OgreBullet bullet OIS`

# Flags del linker ---------------------------------------------------
LDFLAGS := `pkg-config --libs-only-L OGRE` -lstdc++
LDLIBS := `pkg-config --libs-only-l OgreBullet OGRE OGRE-Overlay bullet SDL2_mixer` -lOIS -lGL -lstdc++ -lCEGUIBase-0 -lboost_system -lCEGUIOgreRenderer-0

# Modo de compilación (-mode=release -mode=debug) --------------------
ifeq ($(mode), release)
	CXXFLAGS += -O2 -D_RELEASE
else
	CXXFLAGS += -g -D_DEBUG
	mode := debug
endif

# Obtención automática de la lista de objetos a compilar -------------
OBJS := $(subst $(DIRSRC), $(DIROBJ), $(patsubst %.cpp, %.o, $(wildcard $(DIRSRC)*.cpp)))
OBJS += $(subst $(DIRSRCENTITIES), $(DIROBJ), $(patsubst %.cpp, %.o, $(wildcard $(DIRSRCENTITIES)*.cpp)))
OBJS += $(subst $(DIRSRCSTATES), $(DIROBJ), $(patsubst %.cpp, %.o, $(wildcard $(DIRSRCSTATES)*.cpp)))
OBJS += $(subst $(DIRSRCMANAGERS), $(DIROBJ), $(patsubst %.cpp, %.o, $(wildcard $(DIRSRCMANAGERS)*.cpp)))
OBJS += $(subst $(DIRSRCAUDIO), $(DIROBJ), $(patsubst %.cpp, %.o, $(wildcard $(DIRSRCAUDIO)*.cpp)))

.PHONY: all clean

all: info $(EXEC)

info:
	@echo ""
	@echo "------------------------------------"
	@echo "Construyendo $(EXEC) en modo $(mode)"
	@echo "------------------------------------"
	@echo ""

# Enlazado -----------------------------------------------------------
$(EXEC): $(OBJS)
	@echo "Enlazando: $(notdir $^)"
	@$(CXX) $(LDFLAGS) -o $@ $^ $(LDLIBS)

	@echo "Terminado."
# Compilación --------------------------------------------------------
$(DIROBJ)%.o: $(DIRSRC)%.cpp
	@echo "Compilando: $(notdir $<)"
	@$(CXX) $(CXXFLAGS) -c $< -o $@ $(LDLIBS)

$(DIROBJ)%.o: $(DIRSRCENTITIES)%.cpp
	@echo "Compilando: $(notdir $<)"
	@$(CXX) $(CXXFLAGS) -c $< -o $@ $(LDLIBS)

$(DIROBJ)%.o: $(DIRSRCSTATES)%.cpp
	@echo "Compilando: $(notdir $<)"
	@$(CXX) $(CXXFLAGS) -c $< -o $@ $(LDLIBS)

$(DIROBJ)%.o: $(DIRSRCMANAGERS)%.cpp
	@echo "Compilando: $(notdir $<)"
	@$(CXX) $(CXXFLAGS) -c $< -o $@ $(LDLIBS)

$(DIROBJ)%.o: $(DIRSRCAUDIO)%.cpp
	@echo "Compilando: $(notdir $<)"
	@$(CXX) $(CXXFLAGS) -c $< -o $@ $(LDLIBS)

# Limpieza de temporales ---------------------------------------------
clean:
	rm -f *.log $(EXEC) *~ $(DIROBJ)* $(DIRSRC)*~ $(DIRHEA)*~
