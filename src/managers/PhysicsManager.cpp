#include "PhysicsManager.h"
#include "Floor.h"
#include "LeftWall.h"
#include "RightWall.h"
#include "EntitiesManager.h"
#include "Entity.h"

template <> PhysicsManager* Ogre::Singleton<PhysicsManager>::msSingleton = nullptr;

PhysicsManager::PhysicsManager () : _root(nullptr), _sceneMgr(nullptr), _world(nullptr), _debugDrawer(nullptr), _debug(false) {
    _root = Ogre::Root::getSingletonPtr();
    _sceneMgr = _root->getSceneManager("SceneManager");

    // Creacion del modulo de debug visual de Bullet
    _debugDrawer = new OgreBulletCollisions::DebugDrawer();
    _debugDrawer->setDrawWireframe(true);
    Ogre::SceneNode *debugNode = _sceneMgr->getRootSceneNode()->createChildSceneNode("debugNode", Ogre::Vector3::ZERO);
    debugNode->attachObject(static_cast <Ogre::SimpleRenderable *>(_debugDrawer));

    // Creacion del mundo (definicion de limites y gravedad)
    Ogre::AxisAlignedBox worldBounds = Ogre::AxisAlignedBox (
        Ogre::Vector3(-10000, -10000,-10000),
        Ogre::Vector3(10000, 10000, 10000));
    Ogre::Vector3 gravity = Ogre::Vector3(0, -9.8, 0);

    _world = new OgreBulletDynamics::DynamicsWorld(_sceneMgr, worldBounds, gravity);
    _world->setDebugDrawer(_debugDrawer);
    _world->setShowDebugShapes(_debug);
}

PhysicsManager::~PhysicsManager () {
    removeObjects();

    if (_debugDrawer) {
        delete _debugDrawer;
        _world->setDebugDrawer(nullptr);
    }

    if(_world) {
        delete _world;
    }
}

PhysicsManager* PhysicsManager::getSingletonPtr () {
    return msSingleton;
}

PhysicsManager& PhysicsManager::getSingleton () {
    assert (msSingleton);
    return *msSingleton;
}

void PhysicsManager::update(float delta) {
    _world->stepSimulation(delta);
    checkCollisions();
    removeObjects();
}

void PhysicsManager::checkCollisions() {
    btCollisionWorld *bulletWorld = _world->getBulletCollisionWorld();
    int numManifolds = bulletWorld->getDispatcher()->getNumManifolds();
    for (int i = 0; i < numManifolds; i++) {
        btPersistentManifold *contactManifold = bulletWorld->getDispatcher()->getManifoldByIndexInternal(i);
        int numContacts = contactManifold->getNumContacts();
        if (numContacts > 0) {
            btCollisionObject *obA = (btCollisionObject*)(contactManifold->getBody0());
            btCollisionObject *obB = (btCollisionObject*)(contactManifold->getBody1());
            void *userPtrA = obA->getUserPointer();
            void *userPtrB = obB->getUserPointer();
            Entity *entA = nullptr;
            Entity *entB = nullptr;
            if (userPtrA) {
                entA = static_cast<Entity*>(userPtrA);
            }

            if (userPtrB) {
                entB = static_cast<Entity*>(userPtrB);
            }

            if (entA) {
                entA->onCollision(entB);
            }

            if (entB) {
                entB->onCollision(entA);
            }
        }
    }
}

void PhysicsManager::switchDebug () {
    _debug = !_debug;
    _world->setShowDebugShapes(_debug);
}

void PhysicsManager::addRigidBody(OgreBulletDynamics::RigidBody *body) {
    _bodies.push_back(body);
}

void PhysicsManager::addCollisionShape(OgreBulletCollisions::CollisionShape *shape) {
    _shapes.push_back(shape);
}

void PhysicsManager::removeObjects () {
    std::deque <OgreBulletDynamics::RigidBody *>::iterator itBody = _bodies.begin();
    while (_bodies.end() != itBody) {
        delete *itBody;
        ++itBody;
    }

    std::deque <OgreBulletCollisions::CollisionShape *>::iterator itShapes = _shapes.begin();
    while (_shapes.end() != itShapes) {
        delete *itShapes;
        ++itShapes;
    }

    _bodies.clear();
    _shapes.clear();
}

void PhysicsManager::removeScenario () {
    std::deque <OgreBulletDynamics::RigidBody *>::iterator itBodyScenario = _bodiesScenario.begin();
    while (_bodiesScenario.end() != itBodyScenario) {
        delete *itBodyScenario;
        ++itBodyScenario;
    }

    std::deque <OgreBulletCollisions::CollisionShape *>::iterator itShapesScenario = _shapesScenario.begin();
    while (_shapesScenario.end() != itShapesScenario) {
        delete *itShapesScenario;
        ++itShapesScenario;
    }

    _bodiesScenario.clear();
    _shapesScenario.clear();
}

void PhysicsManager::init() {
    // Create the visual part of the "ground" plane
    Ogre::Plane plane1(Ogre::Vector3(0, 1, 0), 0);
    Ogre::MeshManager::getSingleton().createPlane("p1", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, plane1, 200, 200, 1, 1, true, 1, 20, 20, Ogre::Vector3::UNIT_Z);
    Ogre::SceneNode *nodeGround = _sceneMgr->createSceneNode("ground");
    Ogre::Entity *groundEnt = _sceneMgr->createEntity("groundEnt", "p1");
    groundEnt->setCastShadows(false);
    groundEnt->setMaterialName("Ground");
    nodeGround->attachObject(groundEnt);
    _sceneMgr->getRootSceneNode()->addChild(nodeGround);

    // Create the visual part of the "roof" plane
    Ogre::Plane plane2(Ogre::Vector3(0, -1, 0), -5);
    Ogre::MeshManager::getSingleton().createPlane("p2", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, plane2, 200, 200, 1, 1, true, 1, 20, 20, Ogre::Vector3::NEGATIVE_UNIT_Z);
    Ogre::SceneNode *nodeRoof = _sceneMgr->createSceneNode("roof");
    Ogre::Entity *roofEnt = _sceneMgr->createEntity("roofEnt", "p2");
    roofEnt->setCastShadows(false);
    roofEnt->setMaterialName("Ground");
    nodeRoof->attachObject(roofEnt);
    _sceneMgr->getRootSceneNode()->addChild(nodeRoof);

    // Create the visual part of the left wall
    Ogre::Plane plane3(Ogre::Vector3(-1, 0, 0), -5);
    Ogre::MeshManager::getSingleton().createPlane("p3", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, plane3, 200, 200, 1, 1, true, 1, 20, 20, Ogre::Vector3::UNIT_Z);
    Ogre::SceneNode *nodeLeftWall = _sceneMgr->createSceneNode("leftWall");
    Ogre::Entity *leftWallEnt = _sceneMgr->createEntity("leftWallEnt", "p3");
    leftWallEnt->setCastShadows(false);
    nodeLeftWall->attachObject(leftWallEnt);
    leftWallEnt->setMaterialName("Ground");
    _sceneMgr->getRootSceneNode()->addChild(nodeLeftWall);

    // Create the visual part of the right wall
    Ogre::Plane plane4(Ogre::Vector3(1, 0, 0), -5);
    Ogre::MeshManager::getSingleton().createPlane("p4", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, plane4, 200, 200, 1, 1, true, 1, 20, 20, Ogre::Vector3::UNIT_Z);
    Ogre::SceneNode *nodeRightWall = _sceneMgr->createSceneNode("rightWall");
    Ogre::Entity *rightWallEnt = _sceneMgr->createEntity("rightWallEnt", "p4");
    rightWallEnt->setCastShadows(false);
    nodeRightWall->attachObject(rightWallEnt);
    rightWallEnt->setMaterialName("Ground");
    _sceneMgr->getRootSceneNode()->addChild(nodeRightWall);

    // Create the collision part of the ground
    OgreBulletCollisions::CollisionShape *ShapeGround = new OgreBulletCollisions::StaticPlaneCollisionShape(Ogre::Vector3(0,1,0), 0);
    OgreBulletDynamics::RigidBody *rigidBodyPlaneGround = new OgreBulletDynamics::RigidBody("rigidBodyPlaneGround", _world, Constants::FLOOR, Constants::floor_collides_with);

    // Create the collision part of the roof
    OgreBulletCollisions::CollisionShape *ShapeRoof = new OgreBulletCollisions::StaticPlaneCollisionShape(Ogre::Vector3(0,-1,0), -5);
    OgreBulletDynamics::RigidBody *rigidBodyPlaneRoof = new OgreBulletDynamics::RigidBody("rigidBodyPlaneRoof", _world, Constants::EXTERNALPLANE, Constants::externalPlane_collides_with);

    // Create the collision part of the left wall
    OgreBulletCollisions::CollisionShape *ShapeLeftWall = new OgreBulletCollisions::StaticPlaneCollisionShape(Ogre::Vector3(-1,0,0), -5);
    OgreBulletDynamics::RigidBody *rigidBodyPlaneLeftWall = new OgreBulletDynamics::RigidBody("rigidBodyPlaneLeftWall", _world, Constants::LATERALPLANE, Constants::lateralPlane_collides_with);

    // Create the collision part of the right wall
    OgreBulletCollisions::CollisionShape *ShapeRightWall = new OgreBulletCollisions::StaticPlaneCollisionShape(Ogre::Vector3(1,0,0), -5);
    OgreBulletDynamics::RigidBody *rigidBodyPlaneRightWall = new OgreBulletDynamics::RigidBody("rigidBodyPlaneRightWall", _world, Constants::LATERALPLANE, Constants::lateralPlane_collides_with);

    // Create the static form (shape, restitution and friction) of the ground
    rigidBodyPlaneGround->setStaticShape(ShapeGround, 0.1, 3.0);
    _shapesScenario.push_back(ShapeGround);
    _bodiesScenario.push_back(rigidBodyPlaneGround);
    Floor *floor = new Floor(nodeGround, groundEnt, rigidBodyPlaneGround, ShapeGround);

    // Create the static form (shape, restitution and friction) of the roof
    rigidBodyPlaneRoof->setStaticShape(ShapeRoof, 0.1, 0.001);
    _shapesScenario.push_back(ShapeRoof);
    _bodiesScenario.push_back(rigidBodyPlaneRoof);

    // Create the static form (shape, restitution and friction) of the left wall
    rigidBodyPlaneLeftWall->setStaticShape(ShapeLeftWall, 0.1, 0.001);
    _shapesScenario.push_back(ShapeLeftWall);
    _bodiesScenario.push_back(rigidBodyPlaneLeftWall);
    LeftWall *leftWall = new LeftWall(nodeLeftWall, leftWallEnt, rigidBodyPlaneLeftWall, ShapeLeftWall);

    // Create the static form (shape, restitution and friction) of the right wall
    rigidBodyPlaneRightWall->setStaticShape(ShapeRightWall, 0.1, 0.001);
    _shapesScenario.push_back(ShapeRightWall);
    _bodiesScenario.push_back(rigidBodyPlaneRightWall);
    RightWall *rightWall = new RightWall(nodeRightWall, rightWallEnt, rigidBodyPlaneRightWall, ShapeRightWall);

    EntitiesManager *entMgr = EntitiesManager::getSingletonPtr();
    entMgr->addStaticEntity(floor);
    entMgr->addStaticEntity(leftWall);
    entMgr->addStaticEntity(rightWall);
}
