#include "CameraManager.h"

template<> CameraManager* Ogre::Singleton<CameraManager>::msSingleton = nullptr;

CameraManager* CameraManager::getSingletonPtr () {
    return msSingleton;
}

CameraManager& CameraManager::getSingleton () {
    assert(msSingleton);
    return *msSingleton;
}

void CameraManager::update (float delta) {
    Ogre::Vector3 position = _player->getWorldPosition();
    //position.x -= 1;
    position.y += 0.5;
    _camera->setPosition(position);
    //_camera->moveRelative(Ogre::Vector3(0, 0, 3));
    position = _camera->getPosition();
    if (position.x < -4.9) {
        position = _camera->getPosition();
        position.x = -4.9;
        _camera->setPosition(position);
    }
    else if (position.x > 4.9) {
        position = _camera->getPosition();
        position.x = 4.9;
        _camera->setPosition(position);
    }
    if (_eyehole_nod) {
        _eyehole_nod->setPosition(position);
        _eyehole_nod->translate(Ogre::Vector3(0, 0, -1), Ogre::Node::TS_LOCAL);
        _eyehole_nod->setOrientation(_camera->getOrientation());
    }
}

void CameraManager::clear () {
    if (_player) {
        _player = nullptr;
    }
    if (_eyehole_nod) {
        _eyehole_nod->getParent()->removeChild(_eyehole_nod);
        _eyehole_nod->detachAllObjects();
        Ogre::SceneManager *sceneMgr = Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager");
        sceneMgr->destroySceneNode(_eyehole_nod);
        sceneMgr->destroyEntity(_eyehole_ent);
    }
}
