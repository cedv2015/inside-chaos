#include "ParticlesManager.h"
#include "Constants.h"

template <> ParticlesManager* Ogre::Singleton<ParticlesManager>::msSingleton = nullptr;

ParticlesManager::ParticlesManager () : _id_counter(0) {
}

ParticlesManager::~ParticlesManager () {
    //destroyAllParticles ();
}

void ParticlesManager::update (float delta) {
    int size = _particles.size();
    for (int i = 0; i < size; i++) {
        MyParticleSystem *particle = _particles[i];
        particle->update(delta);
        if (particle->isFinished()) {
            delete particle;
            _particles.erase(_particles.begin() + i);
            i--; size--;
        }
    }
}

MyParticleSystem* ParticlesManager::createParticleSystem(Ogre::Vector3 position, int type) {
    std::string name;
    switch (type) {
        case Particles::GRENADEEXPLOSION:
            name = std::string("Grenade");
            break;
        case Particles::SPARKS:
            name = std::string("Sparks");
            break;
        case Particles::LASER:
            name = std::string("Laser");
            break;
        case Particles::ENEMYEXPLOSION:
            name = std::string("EnemyExplosion");
            break;
        case Particles::ROLLEREXPLOSION:
            name = std::string("RollerExplosion");
            break;
        case Particles::PLAYERBULLET:
            name = std::string("PlayerBullet");
            break;
        default:
            break;
    }
    MyParticleSystem *particle = new MyParticleSystem(position, name, ++_id_counter);
    _particles.push_back(particle);
    return particle;
}

void ParticlesManager::destroyAllParticles () {
    int size = _particles.size();
    for (int i = 0; i < size; ++i) {
        MyParticleSystem *particle = _particles[i];
        delete particle;
    }
    _particles.clear();
}

ParticlesManager* ParticlesManager::getSingletonPtr () {
    return msSingleton;
}

ParticlesManager& ParticlesManager::getSingleton () {
    assert(msSingleton);
    return *msSingleton;
}

