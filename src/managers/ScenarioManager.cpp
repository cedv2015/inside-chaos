#include "ScenarioManager.h"

#include "EntitiesManager.h"
#include "Shapes/OgreBulletCollisionsBoxShape.h"

template <> ScenarioManager* Ogre::Singleton<ScenarioManager>::msSingleton = nullptr;

ScenarioManager::ScenarioManager() : _stage(nullptr), _number_of_stages(0), _pA(nullptr), _pB(nullptr), _pC(nullptr), _openDoor(true), _light(nullptr), _actual_door(0) {
}

ScenarioManager* ScenarioManager::getSingletonPtr () {
    return msSingleton;
}

ScenarioManager& ScenarioManager::getSingleton () {
    assert (msSingleton);
    return *msSingleton;
}

void ScenarioManager::getScene () {
    _openDoor = true;
    _number_of_stages = 0;
    _actual_door = 0;
    _sceneManager = Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager");
    _stage = _sceneManager->createSceneNode("Stage" + std::to_string(_number_of_stages));
    _sceneManager->getRootSceneNode()->addChild(_stage);
    _light = _sceneManager->createLight("MainLight");
    _light->setType(Ogre::Light::LT_SPOTLIGHT);
    _light->setCastShadows(true);
    _light->setDirection(Ogre::Vector3(0, -1, 0));
    _light->setSpotlightInnerAngle(Ogre::Degree(20));
    _light->setSpotlightOuterAngle(Ogre::Degree(90));
    _light->setPosition(0, 10, -100 + Constants::NUMBER_ELEMENTS_PER_SCENARIO * Constants::WIDTH_OF_MODULES * _number_of_stages + Constants::NUMBER_ELEMENTS_PER_SCENARIO * Constants::WIDTH_OF_MODULES / 2);

    createPlanes();
    createDoors();
    generateInitialNodes();
    generateScenarioRandom();
}

void ScenarioManager::generateInitialNodes() {
    for (int i = 0; i < 3; ++i) {
        _nodesScenario.push_back(nullptr);
        _entitiesScenario.push_back(nullptr);
    }
}

void ScenarioManager::newStage(){
    //que no se pueda generar una escena nueva si su puerta correspondiente no ha sido abierta
    if(_openDoor){
        _openDoor = false;
        _number_of_stages++;
        generateScenarioRandom();
        _pB->moveForward(-100 + Constants::WIDTH_OF_MODULES + _number_of_stages * Constants::WIDTH_OF_MODULES * 3);
        _pC->moveForward(-100 + Constants::WIDTH_OF_MODULES * 3 + _number_of_stages * Constants::WIDTH_OF_MODULES * 3);
        _light->setPosition(0, 10, -100 + Constants::NUMBER_ELEMENTS_PER_SCENARIO * Constants::WIDTH_OF_MODULES * _number_of_stages + Constants::NUMBER_ELEMENTS_PER_SCENARIO * Constants::WIDTH_OF_MODULES / 2);

        //si cruza la puerta openDoor();
        _doors[(_actual_door + 1 > 2) ? 0 : (_actual_door + 1)]->open();
    }

}

void ScenarioManager::openDoor(){
    _openDoor = true;
    _pA->moveForward(-101 + _number_of_stages * Constants::WIDTH_OF_MODULES * 3);
}
void ScenarioManager::generateScenarioRandom(){

    int num_aux = rand() % 3 + 1;
    Ogre::Entity *entA = _sceneManager->createEntity("RoomA" + std::to_string(_number_of_stages), "RoomA"+std::to_string(num_aux)+".mesh");
    Ogre::SceneNode *nodeA= _sceneManager->createSceneNode("RoomA_Node" + std::to_string(_number_of_stages));
    _stage->addChild(nodeA);
    nodeA->attachObject(entA);
    nodeA->setPosition(Ogre::Vector3(0, 0.01, -100 + Constants::WIDTH_OF_MODULES / 2 + _number_of_stages * Constants::WIDTH_OF_MODULES * 3));
    nodeA->setScale(Ogre::Vector3(0.99, 0.99, 1.0));
    num_aux = rand() % 3 + 1;
    Ogre::Entity *entB = _sceneManager->createEntity("RoomB" + std::to_string(_number_of_stages), "RoomB"+std::to_string(num_aux)+".mesh");
    Ogre::SceneNode *nodeB= _sceneManager->createSceneNode("RoomB_Node" + std::to_string(_number_of_stages));
    _stage->addChild(nodeB);
    nodeB->attachObject(entB);
    nodeB->setPosition(Ogre::Vector3(0, 0.01, -100 + Constants::WIDTH_OF_MODULES / 2 + _number_of_stages * Constants::WIDTH_OF_MODULES * 3 + Constants::WIDTH_OF_MODULES));
    nodeB->setScale(Ogre::Vector3(0.99, 0.99, 1.0));
    num_aux = rand() % 3 + 1;
    Ogre::Entity *entC = _sceneManager->createEntity("RoomC" + std::to_string(_number_of_stages), "RoomC"+std::to_string(num_aux)+".mesh");
    Ogre::SceneNode *nodeC= _sceneManager->createSceneNode("RoomC_Node" + std::to_string(_number_of_stages));
    _stage->addChild(nodeC);
    nodeC->attachObject(entC);
    nodeC->setPosition(Ogre::Vector3(0, 0.01, -100 + Constants::WIDTH_OF_MODULES / 2 + _number_of_stages * Constants::WIDTH_OF_MODULES * 3 + Constants::WIDTH_OF_MODULES * 2));
    nodeC->setScale(Ogre::Vector3(0.99, 0.99, 1.0));


    _nodesScenario.push_back(nodeA);
    _nodesScenario.push_back(nodeB);
    _nodesScenario.push_back(nodeC);

    _entitiesScenario.push_back(entA);
    _entitiesScenario.push_back(entB);
    _entitiesScenario.push_back(entC);
    for (unsigned int i = 0; i < _entitiesScenario.size(); i++) {
        if (_entitiesScenario[i]) {
            _entitiesScenario[i]->setCastShadows(false);
        }
    }

}

void ScenarioManager::createPlanes(){
    Ogre::Entity *planeA = _sceneManager->createEntity("PlaneA", "Plane.mesh");
    Ogre::SceneNode *planeA_Node= _sceneManager->createSceneNode("PlaneA_Node");
    _stage->addChild(planeA_Node);
    planeA_Node->attachObject(planeA);
    planeA_Node->setPosition(Ogre::Vector3(0, 0, -100 + _number_of_stages * Constants::WIDTH_OF_MODULES * 3));

    Ogre::Entity *planeB = _sceneManager->createEntity("PlaneB", "Plane.mesh");
    Ogre::SceneNode *planeB_Node= _sceneManager->createSceneNode("PlaneB_Node");
    _stage->addChild(planeB_Node);
    planeB_Node->attachObject(planeB);
    planeB_Node->setPosition(Ogre::Vector3(0, 0, -100 + Constants::WIDTH_OF_MODULES + _number_of_stages * Constants::WIDTH_OF_MODULES * 3));

    Ogre::Entity *planeC = _sceneManager->createEntity("PlaneC", "Plane.mesh");
    Ogre::SceneNode *planeC_Node= _sceneManager->createSceneNode("PlaneC_Node");
    _stage->addChild(planeC_Node);
    planeC_Node->attachObject(planeC);
    planeC_Node->setPosition(Ogre::Vector3(0, 0, -100 + Constants::WIDTH_OF_MODULES * 3 + _number_of_stages * Constants::WIDTH_OF_MODULES * 3));

    PhysicsManager *physicsMgr = PhysicsManager::getSingletonPtr();

    Ogre::AxisAlignedBox boundingB = planeA->getBoundingBox();
    Ogre::Vector3 size = boundingB.getSize();
    OgreBulletCollisions::BoxCollisionShape *ShapePlaneA = new OgreBulletCollisions::BoxCollisionShape(size);
    OgreBulletDynamics::RigidBody *rigidBodyPlaneA = new OgreBulletDynamics::RigidBody("rigidBodyPlaneA", physicsMgr->getWorld(), Constants::EXTERNALPLANE, Constants::externalPlane_collides_with);
    rigidBodyPlaneA->setShape(planeA_Node, ShapePlaneA, 0.1, 0.1, 100, Ogre::Vector3(0, 0, -100 + _number_of_stages * Constants::WIDTH_OF_MODULES * 3));

    OgreBulletCollisions::BoxCollisionShape *ShapePlaneB = new OgreBulletCollisions::BoxCollisionShape(size);
    OgreBulletDynamics::RigidBody *rigidBodyPlaneB = new OgreBulletDynamics::RigidBody("rigidBodyPlaneB", physicsMgr->getWorld(), Constants::MIDDLEPLANE, Constants::middlePlane_collides_with);
    rigidBodyPlaneB->setShape(planeB_Node, ShapePlaneB, 0.1, 0.1, 100, Ogre::Vector3(0, 0, -100 + Constants::WIDTH_OF_MODULES + _number_of_stages * Constants::WIDTH_OF_MODULES * 3));

    OgreBulletCollisions::BoxCollisionShape *ShapePlaneC = new OgreBulletCollisions::BoxCollisionShape(size);
    OgreBulletDynamics::RigidBody *rigidBodyPlaneC = new OgreBulletDynamics::RigidBody("rigidBodyPlaneC", physicsMgr->getWorld(), Constants::EXTERNALPLANE, Constants::externalPlane_collides_with);
    rigidBodyPlaneC->setShape(planeC_Node, ShapePlaneC, 0.1, 0.1, 100, Ogre::Vector3(0, 0, -100 + Constants::WIDTH_OF_MODULES * 3 + _number_of_stages * Constants::WIDTH_OF_MODULES * 3));

    EntitiesManager *entMgr = EntitiesManager::getSingletonPtr();

    _pA = new Plane(planeA_Node, planeA, rigidBodyPlaneA, ShapePlaneA);
    entMgr->addDynamicEntity(_pA);
    _pB = new Plane(planeB_Node, planeB, rigidBodyPlaneB, ShapePlaneB);
    entMgr->addDynamicEntity(_pB);
    _pC = new Plane(planeC_Node, planeC, rigidBodyPlaneC, ShapePlaneC);
    entMgr->addDynamicEntity(_pC);
}

void ScenarioManager::createDoors () {
    EntitiesManager *entMgr = EntitiesManager::getSingletonPtr();
    for (int i = 0; i < 3; i++) {
        Door *door = new Door(i);
        _doors.push_back(door);
        entMgr->addDynamicEntity(door);
    }
}

void ScenarioManager::removeAllElements () {
    int size = _nodesScenario.size();
    for (int i = 0; i < size; ++i) {
        if (_nodesScenario[i]) {
            Ogre::SceneNode *node = _nodesScenario[i];
            Ogre::Entity *entity = _entitiesScenario[i];
            node->getParent()->removeChild(node);
            _sceneManager->destroySceneNode(node);
            _sceneManager->destroyEntity(entity);
        }
        _nodesScenario.erase(_nodesScenario.begin(), _nodesScenario.end());
        _entitiesScenario.erase(_entitiesScenario.begin(), _entitiesScenario.end());
    }

    _stage->getParent()->removeChild(_stage);
    _sceneManager->destroySceneNode(_stage);
    _number_of_stages = 0;
    _doors.clear();
}

void ScenarioManager::removeLastScenario() {
    if (_nodesScenario.size() > Constants::NUMBER_ELEMENTS_PER_SCENARIO * 2) {
        for (unsigned int i = 0; i < Constants::NUMBER_ELEMENTS_PER_SCENARIO; i++) {
            Ogre::SceneNode *node = _nodesScenario[i];
            if (node) {
                node->getParent()->removeChild(node);
                node->detachAllObjects();
                _sceneManager->destroySceneNode(node);
                _sceneManager->destroyEntity(_entitiesScenario[i]);
            }
        }
        _nodesScenario.erase(_nodesScenario.begin(), _nodesScenario.begin() + Constants::NUMBER_ELEMENTS_PER_SCENARIO);
        _entitiesScenario.erase(_entitiesScenario.begin(), _entitiesScenario.begin() + Constants::NUMBER_ELEMENTS_PER_SCENARIO);
        _doors[_actual_door]->moveForward();
        _actual_door = (_actual_door + 1 > 2) ? 0 : (_actual_door + 1);
        _doors[_actual_door]->close();
    }
    openDoor();
}
