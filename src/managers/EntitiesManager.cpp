#include "EntitiesManager.h"
#include "ScenarioManager.h"

template <> EntitiesManager* Ogre::Singleton<EntitiesManager>::msSingleton = nullptr;

EntitiesManager::EntitiesManager () : _number_enemies(0), _player(nullptr), _update_enemies(true), _enemyLdr(nullptr) {
    _scenarioMgr = ScenarioManager::getSingletonPtr();
    _enemyLdr = new EnemyLoader();
}

EntitiesManager::~EntitiesManager () {
    delete _enemyLdr;
}

EntitiesManager* EntitiesManager::getSingletonPtr () {
    return msSingleton;
}

EntitiesManager& EntitiesManager::getSingleton () {
    assert(msSingleton);
    return *msSingleton;
}

void EntitiesManager::update (float delta) {
    int size = _entities.size();
    for (int i = 0; i < size; i++) {
        DynamicEntity *ent = _entities[i];
        if (ent->getType() != Type::ENEMY || _update_enemies) {
            ent->update(delta);
            if (ent->isFinished()) {
                if (ent->getType() == Type::ENEMY) {
                    _number_enemies--;
                }
                delete ent;
                _entities.erase(_entities.begin() + i);
                i--; size--;
                if (_number_enemies <= 0) {
                    _scenarioMgr->newStage();
                    createEnemies();
                    _update_enemies = false;
                }
            }
        }
    }

    if (_entities.size() > 0){
        if (_entities[0]->getType() == Type::PLAYER) {
            Player *p = static_cast<Player*>(_entities[0]);
            if (p->getZ() > _scenarioMgr->getThreshold()) {
                _scenarioMgr->removeLastScenario();
                _update_enemies = true;
            }
        }
    }
}

Player* EntitiesManager::createPlayer () {
    _player = new Player;
    _entities.push_back(_player);
    createEnemies();
    return _player;
}

void EntitiesManager::addStaticEntity (StaticEntity *entity) {
    _staticEntities.push_back(entity);
}

void EntitiesManager::addDynamicEntity (DynamicEntity *entity) {
    _entities.push_back(entity);
}

void EntitiesManager::removeEntities () {
    int size = _entities.size();
    for (int i = 0; i < size; i++) {
        DynamicEntity *ent = _entities[i];
        delete ent;
    }

    _entities.clear();

    size = _staticEntities.size();
    for (int i = 0; i < size; i++) {
        StaticEntity *ent = _staticEntities[i];
        delete ent;
    }

    _staticEntities.clear();
}

void EntitiesManager::createEnemies () {
    int zposition = -100 + _scenarioMgr->getNumberOfStages() * Constants::NUMBER_ELEMENTS_PER_SCENARIO * Constants::WIDTH_OF_MODULES + 15;
    std::vector<DynamicEntity*> entities = _enemyLdr->generateEnemies(zposition, static_cast<Player*>(_entities[0]));
    _number_enemies = entities.size();
    _entities.insert(_entities.end(), entities.begin(), entities.end());
}

/*void EntitiesManager::createTurrets (int nturrets) {
    std::vector<int> positions_left = {0, 1, 2, 3, 4, 5};
    int size = 6;
    while (nturrets > 0) {
        int pos_aux = rand() % size;
        int pos = positions_left[pos_aux];
        Turret *t = new Turret(static_cast<Player*>(_entities[0]), -100 + _scenarioMgr->getNumberOfStages() * Constants::NUMBER_ELEMENTS_PER_SCENARIO * Constants::WIDTH_OF_MODULES + Constants::NUMBER_ELEMENTS_PER_SCENARIO * Constants::WIDTH_OF_MODULES - 1, pos, 3, pos);
        _entities.push_back(t);
        nturrets--;
        size--;
        positions_left.erase(positions_left.begin()+pos_aux);
    }
}*/
