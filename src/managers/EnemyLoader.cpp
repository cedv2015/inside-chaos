#include "EnemyLoader.h"
#include "Borderland.h"
#include "Roller.h"
#include "Turret.h"
#include "Constants.h"

template<> EnemyLoader* Ogre::Singleton<EnemyLoader>::msSingleton = nullptr;

EnemyLoader* EnemyLoader::getSingletonPtr () {
    return msSingleton;
}

EnemyLoader& EnemyLoader::getSingleton () {
    assert(msSingleton);
    return *msSingleton;
}

std::vector<DynamicEntity*> EnemyLoader::generateEnemies (float zposition, Player *player) {
    std::vector<DynamicEntity*> entities;
    std::vector<DynamicEntity*> borderlands = loadBorderlands (zposition, player);
    entities.insert(entities.end(), borderlands.begin(), borderlands.end());
    std::vector<DynamicEntity*> rollers = loadRollers (zposition, player);
    entities.insert(entities.end(), rollers.begin(), rollers.end());
    std::vector<DynamicEntity*> turrets = loadTurrets (zposition, player);
    entities.insert(entities.end(), turrets.begin(), turrets.end());
    return entities;
}

std::vector<DynamicEntity*> EnemyLoader::loadBorderlands (float zposition, Player *player) {
    // File reading
    int file_number = rand() % BORDERLAND_FILE_NUMBER;
    std::string file_name = "patterns/borderland/pattern" + std::to_string(file_number);
    std::ifstream file(file_name);
    std::vector<int> elements;
    std::string line;
    if (file.is_open()) {
        std::string item;
        while (getline(file, line)) {
            std::stringstream iss;
            iss << line;
            while (std::getline (iss, item, DELIM)) {
                elements.push_back(std::stoi(item));
            }
        }
        file.close();
    }
    // Creation of enemies depending of the content of the file
    std::vector<DynamicEntity*> entities;
    bool changeDirection = false;
    if (rand() % 2 != 0) {
        changeDirection = !changeDirection;
    }

    int id = 0;
    for (unsigned int i = 0; i < elements.size(); i+=2) {
        bool goingRight = true;
        if (elements[i] == 0) {
            goingRight = false;
        }
        if (changeDirection) {
            goingRight = !goingRight;
        }
        float iddleTime = elements[i+1];
        Borderland *enemy = new Borderland(player, goingRight, zposition, ++id, iddleTime);
        entities.push_back(enemy);
    }
    return entities;
}

std::vector<DynamicEntity*> EnemyLoader::loadRollers (float zposition, Player *player) {
    // File reading
    int file_number = rand() % BORDERLAND_FILE_NUMBER;
    std::string file_name = "patterns/roller/pattern" + std::to_string(file_number);
    std::ifstream file(file_name);
    std::vector<int> elements;
    std::string line;
    if (file.is_open()) {
        std::string item;
        while (getline(file, line)) {
            std::stringstream iss;
            iss << line;
            while (std::getline (iss, item, DELIM)) {
                elements.push_back(std::stoi(item));
            }
        }
        file.close();
    }
    // Creation of enemies depending of the content of the file
    std::vector<DynamicEntity*> entities;
    bool changeDirection = false;
    if (rand() % 2 != 0) {
        changeDirection = !changeDirection;
    }

    int id = 0;
    for (unsigned int i = 0; i < elements.size(); i+=2) {
        bool goingRight = true;
        if (elements[i] == 0) {
            goingRight = false;
        }
        if (changeDirection) {
            goingRight = !goingRight;
        }
        float iddleTime = elements[i+1];
        Roller *enemy = new Roller(player, goingRight, zposition, ++id, iddleTime);
        entities.push_back(enemy);
    }
    return entities;
}

std::vector<DynamicEntity*> EnemyLoader::loadTurrets (float zposition, Player *player) {
    std::vector<int> positions_left = {0, 1, 2, 3, 4, 5};
    std::vector<DynamicEntity*> entities;
    int nturrets = rand() % 3 + 2;
    int size = 6;
    while (nturrets > 0) {
        int pos_aux = rand() % size;
        int pos = positions_left[pos_aux];
        Turret *t = new Turret(player, zposition - 15 + Constants::NUMBER_ELEMENTS_PER_SCENARIO * Constants::WIDTH_OF_MODULES - 1, pos, 3, pos);
        entities.push_back(t);
        nturrets--;
        size--;
        positions_left.erase(positions_left.begin()+pos_aux);
    }
    return entities;
}
