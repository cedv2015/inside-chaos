#include <iostream>

#include "GameManager.h"
#include "MainState.h"
#include "GameOverState.h"

int main() {
    GameManager* game = new GameManager;

    new MainState;
    new MenuState;
    new PlayState;
    new PauseState;
    new InstructionsState;
    new RankingState;
    new GameOverState;
    new CreditsState;
    try {
        game->start(MainState::getSingletonPtr());
    } catch (Ogre::Exception& e) {
        std::cout << e.getFullDescription() << std::endl;
    }

    delete game;

    return 0;
}
