#include "PlayerBullet.h"
#include <OgreParticleSystem.h>
#include <Shapes/OgreBulletCollisionsSphereShape.h>

PlayerBullet::PlayerBullet (int id): Bullet(nullptr, nullptr, nullptr, nullptr, Type::PLAYERBULLET, id), _time(0), _sceneMgr(nullptr), _particle(nullptr) {
    Ogre::Root *root = Ogre::Root::getSingletonPtr();
    _sceneMgr = root->getSceneManager("SceneManager");
    _entity = _sceneMgr->createEntity("PlayerBullet"+std::to_string(getId()), "PlayerBullet.mesh");
    _node = _sceneMgr->createSceneNode("PlayerBullet"+std::to_string(getId())+"Node");
    _sceneMgr->getRootSceneNode()->addChild(_node);
    _node->attachObject(_entity);

    PhysicsManager *physicsMgr = PhysicsManager::getSingletonPtr();

    _shape = new OgreBulletCollisions::SphereCollisionShape(0.1);
    _body = new OgreBulletDynamics::RigidBody("rigidBodyPlayerBullet"+std::to_string(getId()), physicsMgr->getWorld(), Constants::PLAYERBULLET, Constants::playerBullet_collides_with);
    Ogre::Camera *camera = _sceneMgr->getCamera("MainCamera");
    Ogre::Vector3 position = (camera->getDerivedPosition() + camera->getDerivedDirection().normalisedCopy());
    if(!_body->getShape())
        _body->setShape(_node, _shape, 5, 0.1, 1.0, position, camera->getOrientation());
    else{
        btTransform transform;
        transform.setIdentity();
        transform.setOrigin(OgreBulletCollisions::OgreBtConverter::to(position));
        _body->getBulletRigidBody()->setWorldTransform(transform);
    }
    _body->setLinearVelocity(camera->getDerivedDirection().normalisedCopy() * 25.0f );
    auto bullet_body = _body->getBulletRigidBody();
    bullet_body->setGravity(btVector3(0, 0, 0));
    bullet_body->setUserPointer(this);
    _particle = ParticlesManager::getSingletonPtr()->createParticleSystem(_body->getWorldPosition(), Particles::PLAYERBULLET);
}

void PlayerBullet::update (float delta) {
    _particle->setPosition(_body->getWorldPosition());
    _time += delta;
    if(_time > 1.0)
        _remove = true;
    return;
}

void PlayerBullet::onCollision(Entity *entity) {
    if (entity) {
        if (entity->getType() == Type::PLANE) {
            _remove = true;
        }
        if (entity->getType() == Type::ROLLERBULLET) {
            _remove = true;
        }
        if (entity->getType() == Type::ENEMY) {
            _remove = true;
        }
        ParticlesManager::getSingletonPtr()->createParticleSystem(_body->getWorldPosition(), Particles::SPARKS);
    }
}
