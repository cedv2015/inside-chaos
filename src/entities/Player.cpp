#include "Player.h"
#include "Constants.h"
#include "PhysicsManager.h"
#include "ScenarioManager.h"

Player::Player () : DynamicEntity(nullptr, nullptr, nullptr, nullptr, Type::PLAYER), _life(Constants::LIFEPLAYER), _points(0), _left(false), _right(false), _up(false), _onGround(false), _onLeftWall(false), _onRightWall(false), _lastFrameUp(false), _forward(false), _back(false), _bleft(false), _lastclicked(false), _onBackWall(false), _throwGrenade(false), _dead(false), _hurt(false), _pistol(nullptr), _machinegun(nullptr), _weapon(nullptr), _grenadeLauncher(nullptr), _rotation(0)/*, _sound_grenade_launch(nullptr)*/, _actual_weapon(0)  {
    Ogre::Root *root = Ogre::Root::getSingletonPtr();
    Ogre::SceneManager *sceneMgr = root->getSceneManager("SceneManager");
    _entity = sceneMgr->createEntity("Player", "Player.mesh");
    _node = sceneMgr->createSceneNode("PlayerNode");
    sceneMgr->getRootSceneNode()->addChild(_node);
    _node->attachObject(_entity);

    PhysicsManager *physicsMgr = PhysicsManager::getSingletonPtr();

    _shape = new OgreBulletCollisions::CapsuleCollisionShape(0.5, 0.9, Ogre::Vector3::UNIT_Y);
    _body = new OgreBulletDynamics::RigidBody("rigidBodyPlayer", physicsMgr->getWorld(), Constants::PLAYER, Constants::player_collides_with);


    _body->setShape(_node, _shape, 0.01, 1, 90.0, Ogre::Vector3(0, 0.9, -97));
    _body->disableDeactivation();
    auto bullet_body = _body->getBulletRigidBody();
    bullet_body->setAngularFactor(btVector3(0, 0, 0));
    bullet_body->setUserPointer(this);
    _pistol = new Pistol(Ogre::Vector3(0, 0.9, -97));
    _machinegun = new Machinegun(Ogre::Vector3(0, 0.9, -97));
    _pistol->swapVisibility();
    _weapon = _pistol;
    _grenadeLauncher = new GrenadeLauncher;
    SoundFXManager *soundFXMgr = SoundFXManager::getSingletonPtr();
    _sound_jump = soundFXMgr->load("jump.wav");
    /*
    _sound_grenade_launch = soundFXMgr->load("grenade_launch.wav");
    */
}

void Player::update (float delta) {
    if (_body) {
        if (_body->getLinearVelocity().squaredLength() < 15.0) {
            if (_left) {
                float x = Constants::PLAYER_SPEED * Ogre::Math::Cos(_rotation);
                float z = Constants::PLAYER_SPEED * Ogre::Math::Sin(_rotation);
                _body->applyImpulse(Ogre::Vector3(x , 0.0 , -z), _body->getCenterOfMassPosition());
            }
            if (_right) {
                float x = Constants::PLAYER_SPEED * Ogre::Math::Cos(_rotation);
                float z = Constants::PLAYER_SPEED * Ogre::Math::Sin(_rotation);
                _body->applyImpulse(Ogre::Vector3(-x , 0.0 , z), _body->getCenterOfMassPosition());
            }
            if (_forward) {
                float x = Constants::PLAYER_SPEED * Ogre::Math::Sin(_rotation);
                float z = Constants::PLAYER_SPEED * Ogre::Math::Cos(_rotation);
                _body->applyImpulse(Ogre::Vector3(x , 0.0 , z), _body->getCenterOfMassPosition());
            }
            if (_back) {
                float x = Constants::PLAYER_SPEED * Ogre::Math::Sin(_rotation);
                float z = Constants::PLAYER_SPEED * Ogre::Math::Cos(_rotation);
                _body->applyImpulse(Ogre::Vector3(-x , 0.0 , -z), _body->getCenterOfMassPosition());
            }
        }
        if (_up) {
            if (!_lastFrameUp) {
                jump();
            }
        }
        if (_throwGrenade) {
            throwGrenade();
        }
    }

    _weapon->update(delta);
    if (_bleft) {
        _weapon->shoot(_lastclicked);
        if (!_lastclicked){
            _lastclicked = true;
        }
    }else{
        _lastclicked = false;
    }
    _lastFrameUp = _up;
    _onGround = _onRightWall = _onLeftWall = _onBackWall = _throwGrenade = false;
}

void Player::onCollision (Entity *entity) {
    if (entity) {
        if (entity->getType() == Type::FLOOR) {
            _onGround = true;
        }
        else if (entity->getType() == Type::RIGHTWALL) {
            _onRightWall = true;
        }
        else if (entity->getType() == Type::LEFTWALL) {
            _onLeftWall = true;
        }
        else if (entity->getType() == Type::PLANE) {
            //entity->getEntity()->setMaterialName("Plano2");
        }else if (entity->getType() == Type::TURRETBULLET || entity->getType() == Type::ROLLERBULLET || entity->getType() == Type::BORDERLANDBULLET) {
              _hurt = true;
              reduceLife(Constants::REDUCELIFEPLAYER);
              if(getLife()<=0)
                  _dead = false;
                  // _remove de jugador nunca se pondrá a true, se avisará a PlayState para que cambie de estado o algo así, pero no podemos dejar que el jugador se elimine
                  // hasta que se salga del estado PlayState
                  //_remove = true; GAME OVER
        }
    }
}

void Player::jump () {
    if (_onGround || _onLeftWall || _onRightWall) {
        Ogre::Vector3 _actualVelocity = _body->getLinearVelocity();
        if (_onGround) {
            _actualVelocity.y = 6.0;
        }
        else if (_onRightWall) {
            _actualVelocity.y = 3.0;
            _actualVelocity.x = 7.0;
        }
        else if (_onLeftWall) {
            _actualVelocity.y = 3.0;
            _actualVelocity.x = -7.0;
        }
        _body->setLinearVelocity(_actualVelocity);
        //jump sound
        _sound_jump->play(0);
    }
}

float Player::getZ() {
    return _body->getWorldPosition().z;
}

Ogre::Vector3 Player::getPosition() {
    return Ogre::Vector3(_body->getWorldPosition().x, _body->getWorldPosition().y, _body->getWorldPosition().z);
}

void Player::throwGrenade () {
    _grenadeLauncher->shoot(true);
    /*
    _sound_grenade_launch->play(0);
    */
}

void Player::swapWeapon() {
    _weapon->swapVisibility();
    if (_actual_weapon == 0) {
        _actual_weapon = 1;
        _weapon = _machinegun;
    }
    else {
        _actual_weapon = 0;
        _weapon = _pistol;
    }
    _weapon->swapVisibility();
}

int Player::getGrenadesLeft(){
    GrenadeLauncher *g = static_cast<GrenadeLauncher*>(_grenadeLauncher);
    return g->getGrenadesLeft();
}

void Player::hideWeapon(){
    _weapon->swapVisibility();
}
