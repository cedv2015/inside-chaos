#include "GrenadeLauncher.h"
#include "Grenade.h"
#include "EntitiesManager.h"

GrenadeLauncher::GrenadeLauncher () : Weapon (), _grenades_left(3) {
    _entitiesMgr = EntitiesManager::getSingletonPtr();
}

void GrenadeLauncher::shoot(bool last_clicked) {
    if (_grenades_left > 0) {
        Grenade *grenade = new Grenade();
        _entitiesMgr->addDynamicEntity(grenade);
        --_grenades_left;
    }
}

void GrenadeLauncher::update (float delta) {
    return;
}

void GrenadeLauncher::swapVisibility() {

}
