#include "Turret.h"
#include "PhysicsManager.h"
#include "Constants.h"
#include "EntitiesManager.h"
#include "TurretBullet.h"
#include "ParticlesManager.h"
#include "PlayState.h"
#include "Shapes/OgreBulletCollisionsBoxShape.h"

Turret::Turret(Player *player, float zPosition, int id, float iddleTime, int position) : Enemy(nullptr, nullptr, nullptr, nullptr, player, Type::ENEMY, iddleTime), _timeSinceLastShoot(0.0), _timeLive(15.0), _id(id), _skeleton(nullptr) {
    Ogre::Root *root = Ogre::Root::getSingletonPtr();
    Ogre::SceneManager *sceneMgr = root->getSceneManager("SceneManager");
    _entity = sceneMgr->createEntity("Turret" + std::to_string(id), "Turret.mesh");
    _entity->setCastShadows(true);
    _node = sceneMgr->createSceneNode("TurretNode" + std::to_string(id));
    sceneMgr->getRootSceneNode()->addChild(_node);
    _node->attachObject(_entity);
    _skeleton = _entity->getSkeleton();
    _skeleton->getBone("Horizontal")->setManuallyControlled(true);
    _skeleton->getBone("Vertical")->setManuallyControlled(true);

    PhysicsManager *physicsMgr = PhysicsManager::getSingletonPtr();

    _shape = new OgreBulletCollisions::BoxCollisionShape(Ogre::Vector3(0.5,0.5,0.5));
    _body = new OgreBulletDynamics::RigidBody("rigidBodyTurret" + std::to_string(id), physicsMgr->getWorld(), Constants::ENEMY, Constants::enemy_collides_with);

    Ogre::Vector3 initialPosition = Ogre::Vector3::ZERO; // Esta posicion es una de las 6 posiciones fijas
    switch (position) {
        case 0:
            initialPosition = Ogre::Vector3(-2, 2.9, zPosition);
            break;
        case 1:
            initialPosition = Ogre::Vector3(0, 2.9, zPosition);
            break;
        case 2:
            initialPosition = Ogre::Vector3(2, 2.9, zPosition);
            break;
        case 3:
            initialPosition = Ogre::Vector3(-2, 1.5, zPosition);
            break;
        case 4:
            initialPosition = Ogre::Vector3(-0, 1.5, zPosition);
            break;
        case 5:
            initialPosition = Ogre::Vector3(2, 1.5, zPosition);
            break;
        default:
            break;
    }

    _body->setShape(_node, _shape, 0.01, 1, 90.0, initialPosition);
    _body->disableDeactivation();
    auto bullet_body = _body->getBulletRigidBody();
    bullet_body->setAngularFactor(btVector3(0,0,0));
    bullet_body->setLinearFactor(btVector3(0,0,0));
    bullet_body->setUserPointer(this);
    _entitiesMgr = EntitiesManager::getSingletonPtr();
    SoundFXManager *soundFXMgr = SoundFXManager::getSingletonPtr();
    _sound_shoot = soundFXMgr->load("laser.wav");
}

void Turret::update (float delta) {
    if(_timeLive > 0.0){
      _timeLive -= delta;
    }
    else{
      //dissapear();
    }
    if (_iddleTime > 0) {
        _iddleTime -= delta;
    }
    else {
        run();
        shoot(delta);
    }
}

void Turret::shoot (float delta) {
    _timeSinceLastShoot += delta;
    Ogre::Vector3 position_player = _entitiesMgr->getPlayer()->getPosition();
    if (_timeSinceLastShoot > Constants::TIMESHOOTTURRET) {
        Ogre::Vector3 position = _body->getWorldPosition();
        position.y += 0.37;
        _entitiesMgr->addDynamicEntity(new TurretBullet(position, _turretBulletsId, position_player));
        _turretBulletsId++;
        _timeSinceLastShoot = rand() % 15 - 7;
        _timeSinceLastShoot /= 10;
        _sound_shoot->play(0);
    }
}

// In the turret, this method updates his skeleton to face the player
void Turret::run () {
    Ogre::Vector3 position_player = _entitiesMgr->getPlayer()->getPosition();
    Ogre::Vector3 actual_position = Ogre::Vector3(_body->getWorldPosition().x, _body->getWorldPosition().y, _body->getWorldPosition().z);
    // Calculate the yaw
    Ogre::Bone *horiz = _skeleton->getBone("Horizontal");
    Ogre::Bone *vert = _skeleton->getBone("Vertical");
    horiz->resetOrientation();
    vert->resetOrientation();
    {
        Ogre::Vector2 position_player2d = Ogre::Vector2(position_player.x, position_player.z);
        Ogre::Vector2 turret_position = Ogre::Vector2(actual_position.x, actual_position.z);
        Ogre::Vector2 direction = (position_player2d - turret_position).normalisedCopy();
        Ogre::Vector2 initial = Ogre::Vector2(1, 0);
        Ogre::Radian angle = initial.angleBetween(direction);
        horiz->yaw(Ogre::Degree(-270));
        horiz->yaw(angle);
    }
    {
        Ogre::Vector2 position_player2d = Ogre::Vector2(position_player.y, position_player.z);
        Ogre::Vector2 turret_position = Ogre::Vector2(actual_position.y, actual_position.z);
        Ogre::Vector2 direction = (position_player2d - turret_position).normalisedCopy();
        Ogre::Vector2 initial = Ogre::Vector2(0, 1);
        Ogre::Radian angle = initial.angleBetween(direction);
        vert->pitch(Ogre::Degree(-180));
        vert->pitch(angle);
    }
}

void Turret::dissapear () {
    ParticlesManager::getSingletonPtr()->createParticleSystem(_body->getWorldPosition(), Particles::ENEMYEXPLOSION);
    _remove = true;
}

void Turret::onCollision (Entity *entity) {
    if (entity) {
        if (entity->getType() == Type::PLAYERBULLET) {
            reduceLife(Constants::REDUCELIFEENEMY);
            if(getLife()<=0){
                dissapear();
                PlayState::getSingletonPtr()->updatePoints(Constants::POINTSTURRET);
            }
        }
        if (entity->getType() == Type::GRENADEEXPLOSION) {
            PlayState::getSingletonPtr()->updatePoints(Constants::POINTSTURRET);
            dissapear();
        }
    }
}

int Turret::_turretBulletsId = 0;
