#include "Plane.h"

void Plane::moveForward(int i){
    Ogre::Vector3 position = Ogre::Vector3(0,0,i);
    btTransform transform;
    transform.setIdentity();
    transform.setOrigin(OgreBulletCollisions::OgreBtConverter::to(position));
    _body->getBulletRigidBody()->setWorldTransform(transform);
    _node->setPosition(position);
    auto bullet_body = _body->getBulletRigidBody();
    bullet_body->setUserPointer(this);
}

void Plane::onCollision (Entity *entity) {
    if (entity) {
        if (entity->getType() == Type::PLAYER) {
            _hide = false;
        }
    }
}

void Plane::update (float delta) {
    if (_hide) {
        _entity->setMaterialName("Plano");
    }else
        _entity->setMaterialName("Plano2");
        _hide = true;
}
