#include "RollerBullet.h"
#include <Shapes/OgreBulletCollisionsCylinderShape.h>
#include "ParticlesManager.h"

RollerBullet::RollerBullet (Ogre::Vector3 initial_position, int id) : Bullet(nullptr, nullptr, nullptr, nullptr, Type::ROLLERBULLET, id) {
    Ogre::Root *root = Ogre::Root::getSingletonPtr();
    Ogre::SceneManager *sceneMgr = root->getSceneManager("SceneManager");
    _entity = sceneMgr->createEntity("RollerBullet" + std::to_string(getId()), "RollerBullet.mesh");
    _node = sceneMgr->createSceneNode("RollerBulletNode" + std::to_string(getId()));
    sceneMgr->getRootSceneNode()->addChild(_node);
    _node->attachObject(_entity);
    _entity->setCastShadows(true);

    PhysicsManager *physicsMgr = PhysicsManager::getSingletonPtr();

    // CylinderCollisionShape(Ogre::Vector3(height, radius, idk), axis)
    _shape = new OgreBulletCollisions::CylinderCollisionShape(Ogre::Vector3(0.5,0.5,0.5), Ogre::Vector3::UNIT_X);
    _body = new OgreBulletDynamics::RigidBody("rigidBodyRollerBullet" + std::to_string(getId()), physicsMgr->getWorld(), Constants::ROLLERBULLET, Constants::rollerBullet_collides_with);

    _body->setShape(_node, _shape, 0.0, 1, 1, initial_position);
    _body->setLinearVelocity(Ogre::Vector3(0, 0, -4));
    _body->disableDeactivation();
    auto bullet_body = _body->getBulletRigidBody();
    bullet_body->setUserPointer(this);
}

void RollerBullet::update (float delta) {
    return;
}

void RollerBullet::onCollision (Entity *entity) {
    if (entity) {
        if (entity->getType() == Type::PLAYERBULLET) {
            ParticlesManager::getSingletonPtr()->createParticleSystem(_body->getWorldPosition(), Particles::ROLLEREXPLOSION);
            _remove = true;
        }
        else if (entity->getType() == Type::PLANE) {
            _remove = true;
        }
        else if (entity->getType() == Type::PLAYER) {
            _remove = true;
        }
        if (entity->getType() == Type::GRENADEEXPLOSION) {
            ParticlesManager::getSingletonPtr()->createParticleSystem(_body->getWorldPosition(), Particles::ROLLEREXPLOSION);
            _remove = true;
        }
    }
}
