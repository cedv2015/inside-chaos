#include "BorderlandBullet.h"
#include "Shapes/OgreBulletCollisionsBoxShape.h"

BorderlandBullet::BorderlandBullet (Ogre::Vector3 initial_position, int id, Ogre::Vector3 position_player) : Bullet(nullptr, nullptr, nullptr, nullptr, Type::BORDERLANDBULLET, id), _particle(nullptr) {
    Ogre::Root *root = Ogre::Root::getSingletonPtr();
    Ogre::SceneManager *sceneMgr = root->getSceneManager("SceneManager");
    _entity = sceneMgr->createEntity("BorderlandBullet" + std::to_string(getId()), "LaserBullet.mesh");
    _node = sceneMgr->createSceneNode("BorderlandBulletNode" + std::to_string(getId()));
    sceneMgr->getRootSceneNode()->addChild(_node);
    _node->attachObject(_entity);

    PhysicsManager *physicsMgr = PhysicsManager::getSingletonPtr();

    Ogre::AxisAlignedBox boundingB = _entity->getBoundingBox();
    Ogre::Vector3 size = boundingB.getSize()/2;

    _shape = new OgreBulletCollisions::BoxCollisionShape(size);
    _body = new OgreBulletDynamics::RigidBody("rigidBodyBorderlandBullet" + std::to_string(getId()), physicsMgr->getWorld(), Constants::BORDERLANDBULLET, Constants::enemyBullet_collides_with);

    Ogre::Vector3 init = Ogre::Vector3::UNIT_Z;
    Ogre::Vector3 dest = position_player-initial_position;
    Ogre::Quaternion quat = init.getRotationTo(dest);

    _body->setShape(_node, _shape, 0.0, 1, 1, initial_position, quat);
    _body->setLinearVelocity((position_player-initial_position).normalisedCopy() *Constants::BORDERLANDBULLETVELOCITY);
    _body->disableDeactivation();
    auto bullet_body = _body->getBulletRigidBody();
    bullet_body->setUserPointer(this);
    bullet_body->setGravity(btVector3(0, 0, 0));
    _particle = ParticlesManager::getSingletonPtr()->createParticleSystem(_body->getWorldPosition(), Particles::LASER);
}

void BorderlandBullet::update (float delta) {
    _particle->setPosition(_body->getWorldPosition());
    return;
}

void BorderlandBullet::onCollision (Entity *entity) {
    if (entity) {
        if (entity->getType() == Type::PLAYERBULLET) {
            _remove = true;
        }
        else if (entity->getType() == Type::PLANE) {
            _remove = true;
        }
        else if (entity->getType() == Type::PLAYER) {
            _remove = true;
        }
    }
}
