#include "Borderland.h"
#include "PhysicsManager.h"
#include "Constants.h"
#include "BorderlandBullet.h"
#include "EntitiesManager.h"
#include "ParticlesManager.h"
#include "PlayState.h"

Borderland::Borderland(Player *player, bool goingRight, float zPosition, int id, float iddleTime) : Enemy(nullptr, nullptr, nullptr, nullptr, player, Type::ENEMY, iddleTime), _goingRight(goingRight), _goForward(true), _lateral(true), _turn(true), _cerrojo(true), _directionChangesLeft(1), _directionChangesBack(1), _timeSinceLastShoot(0.0), _timeForward(0.0), _timeAnimation(0.0), _id(id), _continue(1), _animation(nullptr), _skeleton(nullptr) {
    Ogre::Root *root = Ogre::Root::getSingletonPtr();
    Ogre::SceneManager *sceneMgr = root->getSceneManager("SceneManager");
    _entity = sceneMgr->createEntity("Borderland" + std::to_string(id), "Borderland.mesh");
    _node = sceneMgr->createSceneNode("BorderlandNode" + std::to_string(id));
    sceneMgr->getRootSceneNode()->addChild(_node);
    _node->attachObject(_entity);

    PhysicsManager *physicsMgr = PhysicsManager::getSingletonPtr();

    _shape = new OgreBulletCollisions::CapsuleCollisionShape(0.5, 1.5, Ogre::Vector3::UNIT_Y);
    _body = new OgreBulletDynamics::RigidBody("rigidBodyBorderland" + std::to_string(id), physicsMgr->getWorld(), Constants::ENEMY, Constants::enemy_collides_with);

    Ogre::Vector3 initialPosition;
    if (_goingRight) {
        initialPosition = Ogre::Vector3(6, 0.9, zPosition);
        _state = States::Q0;
    }
    else {
        initialPosition = Ogre::Vector3(-6, 0.9, zPosition);
        _state = States::Q1;
    }

    _body->setShape(_node, _shape, 0.01, 1, 90.0, initialPosition);
    _body->disableDeactivation();
    auto bullet_body = _body->getBulletRigidBody();
    bullet_body->setAngularFactor(btVector3(0,0,0));
    bullet_body->setUserPointer(this);
    _zInitial = zPosition;
    _entitiesMgr = EntitiesManager::getSingletonPtr();
    _animation = new AnimationBlender(_entity);
    SoundFXManager *soundFXMgr = SoundFXManager::getSingletonPtr();
    _sound_die = soundFXMgr->load("shit.wav");
    _sound_shoot = soundFXMgr->load("laser.wav");
}

void Borderland::update (float delta) {
    _animation->addTime(delta);


    if (_iddleTime > 0) {
        _iddleTime -= delta;
    }
    else {
        shoot(delta);
        run(delta);
        animation(delta);
    }
}

void Borderland::shoot (float delta) {
    _timeSinceLastShoot += delta;
    Ogre::Vector3 position_player = _entitiesMgr->getPlayer()->getPosition();
    if (_timeSinceLastShoot > Constants::TIMESHOOTBORDERLAND) {
        _entitiesMgr->addDynamicEntity(new BorderlandBullet(_body->getWorldPosition(), _bulletsBorderlandId, position_player));
        _bulletsBorderlandId++;
        _timeSinceLastShoot = 0;
        _sound_shoot->play(0);
    }
}

void Borderland::animation (float delta){
    _skeleton = _entity->getSkeleton();
    _skeleton->getBone("Head")->setManuallyControlled(true);
    _skeleton->getBone("Body")->setManuallyControlled(true);

    switch(_state){
      case States::Q0:
          if(_cerrojo){
              _skeleton->getBone("Body")->yaw(Ogre::Degree(90), Ogre::Node::TS_LOCAL);
              _cerrojo = false;
              _animation->blend("Run", AnimationBlender::Blend, 24.0, true);
          }

          break;
      case States::Q1:
          if(_cerrojo){
              _skeleton->getBone("Body")->yaw(Ogre::Degree(-90), Ogre::Node::TS_LOCAL);
              _cerrojo = false;
              _animation->blend("Run", AnimationBlender::Blend, 0.5, true);
          }

          break;
      case States::Q2:
          if(_cerrojo){
              if(_goingRight)
                  _skeleton->getBone("Body")->yaw(Ogre::Degree(-90), Ogre::Node::TS_LOCAL);
              else
                  _skeleton->getBone("Body")->yaw(Ogre::Degree(90), Ogre::Node::TS_LOCAL);
              _cerrojo = false;
          }
          _animation->blend("Run", AnimationBlender::Blend, 0.5, true);
          break;
      case States::Q3:
          _animation->blend("Turn", AnimationBlender::Switch, 0.5, false);
          _cerrojo = true;
          _state=States::Q0;
          break;
      case States::Q4:
          _animation->blend("Turn", AnimationBlender::Switch, -0.5, false);
          _cerrojo = true;
          _state=States::Q1;
          break;
      case States::Q5:
          if(_cerrojo){
              _skeleton->getBone("Body")->yaw(Ogre::Degree(180), Ogre::Node::TS_LOCAL);
              _cerrojo = false;
          }
          _animation->blend("Run", AnimationBlender::Blend, 24.0, true);

          break;
      case States::Q6:
          if(_cerrojo){
              _skeleton->getBone("Body")->yaw(Ogre::Degree(180), Ogre::Node::TS_LOCAL);
              _cerrojo = false;
          }
          _animation->blend("Run", AnimationBlender::Blend, 24.0, true);
          break;
    }



    Ogre::Vector3 position_player = Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager")->getCamera("MainCamera")->getPosition();
    Ogre::Vector3 actual_position = Ogre::Vector3(_body->getWorldPosition().x, _body->getWorldPosition().y, _body->getWorldPosition().z);

    Ogre::Bone *horiz = _skeleton->getBone("Head");
    horiz->resetOrientation();
    {
        Ogre::Vector2 position_player2d = Ogre::Vector2(position_player.x, position_player.z);
        Ogre::Vector2 turret_position = Ogre::Vector2(actual_position.x, actual_position.z);
        Ogre::Vector2 direction = (position_player2d - turret_position).normalisedCopy();
        Ogre::Vector2 initial = Ogre::Vector2(1, 0);
        Ogre::Radian angle = initial.angleBetween(direction);
        horiz->pitch(Ogre::Degree(-30));
        horiz->roll(Ogre::Degree(-90));
        horiz->roll(angle);
    }

}

void Borderland::run (float delta) {
    int time_random = 1+rand()%2;
    _timeForward += delta;
    if (_timeForward > time_random) {
        if ((_body->getWorldPosition().x < 4) && (_body->getWorldPosition().x > -4)){
            _lateral = false;
            _continue--;
            if(_continue==0){
                _cerrojo = true;
            }
            _state = States::Q2;
        }
    }

    int limit_random = 4+rand()%8;

    if(_lateral){

        if (_goingRight) {
            _body->setLinearVelocity(Ogre::Vector3(-4.0, 0.0, 0.0));
            if (_directionChangesLeft > 0) {
                if (_body->getWorldPosition().x < -3.5) {
                    //Giro de 180º
                    _state = States::Q5;
                    _goingRight = false;
                    _directionChangesLeft--;
                    _cerrojo=true;
                }
            }
            else if (_body->getWorldPosition().x < -5.5) {
              dissapear();
            }
        }
        else {
            _body->setLinearVelocity(Ogre::Vector3(4.0, 0.0, 0.0));
            if (_directionChangesLeft > 0) {
                if (_body->getWorldPosition().x > 3.5) {
                    //Giro de 180º
                    _state = States::Q6;
                    _goingRight = true;
                    _directionChangesLeft--;
                    _cerrojo=true;
                }
            }
            else if (_body->getWorldPosition().x > 5.5) {
                dissapear();
            }
        }
    } else {
          if(_goForward){
              _body->setLinearVelocity(Ogre::Vector3(0.0, 0.0, -4.0));
              if (_body->getWorldPosition().z < (_zInitial-limit_random)) {
                  _goForward = false;
                  //Camina hacia atras
              }
          }else{
              _body->setLinearVelocity(Ogre::Vector3(0.0, 0.0, 4.0));
                  if (_body->getWorldPosition().z > _zInitial) {
                      //Ha terminado de caminar hacia atras y camina en lateral
                      //Poner contador
                      _lateral = true;
                      _goForward = true;
                      _timeForward = 0;
                      _continue=1;
                      if(_goingRight) _state = States::Q3;
                      else _state = States::Q4;
                  }
          }

    }

}

void Borderland::dissapear () {
    _remove = true;

}

void Borderland::onCollision (Entity *entity) {
    if (entity) {
        if (entity->getType() == Type::PLAYERBULLET) {
            reduceLife(Constants::REDUCELIFEENEMY);
            if(getLife()<=0){
                _sound_die->play(0);
                dissapear();
                PlayState::getSingletonPtr()->updatePoints(Constants::POINTSBORDERLAND);
                ParticlesManager::getSingletonPtr()->createParticleSystem(_body->getWorldPosition(), Particles::ENEMYEXPLOSION);
            }

        }
        if (entity->getType() == Type::GRENADEEXPLOSION) {
            ParticlesManager::getSingletonPtr()->createParticleSystem(_body->getWorldPosition(), Particles::ENEMYEXPLOSION);
            _sound_die->play(0);
            PlayState::getSingletonPtr()->updatePoints(Constants::POINTSBORDERLAND);
            dissapear();
        }
    }
}

int Borderland::_bulletsBorderlandId = 0;
