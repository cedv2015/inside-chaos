#include "Roller.h"
#include "PhysicsManager.h"
#include "Constants.h"
#include "RollerBullet.h"
#include "EntitiesManager.h"
#include "ParticlesManager.h"
#include "PlayState.h"

Roller::Roller(Player *player, bool goingRight, float zPosition, int id, float iddleTime) : Enemy(nullptr, nullptr, nullptr, nullptr, player, Type::ENEMY, iddleTime), _goingRight(goingRight), _directionChangesLeft(1), _timeSinceLastShoot(0.0), _id(id), _animation(nullptr), _skeleton(nullptr) {
    Ogre::Root *root = Ogre::Root::getSingletonPtr();
    Ogre::SceneManager *sceneMgr = root->getSceneManager("SceneManager");
    _entity = sceneMgr->createEntity("Roller" + std::to_string(id), "Robot.mesh");
    _node = sceneMgr->createSceneNode("RollerNode" + std::to_string(id));
    sceneMgr->getRootSceneNode()->addChild(_node);
    _node->attachObject(_entity);
    _node->scale(0.5, 0.5, 0.5);
    _entity->setCastShadows(true);

    PhysicsManager *physicsMgr = PhysicsManager::getSingletonPtr();

    _shape = new OgreBulletCollisions::CapsuleCollisionShape(0.5, 0.9, Ogre::Vector3::UNIT_Y);
    _body = new OgreBulletDynamics::RigidBody("rigidBodyRoller" + std::to_string(id), physicsMgr->getWorld(), Constants::ENEMY, Constants::enemy_collides_with);

    Ogre::Vector3 initialPosition;
    if (_goingRight) {
        initialPosition = Ogre::Vector3(-6, 0.9, zPosition);
    }
    else {
        initialPosition = Ogre::Vector3(6, 0.9, zPosition);
    }

    _body->setShape(_node, _shape, 0.01, 1, 90.0, initialPosition);
    _body->disableDeactivation();
    auto bullet_body = _body->getBulletRigidBody();
    bullet_body->setAngularFactor(btVector3(0,0,0));
    bullet_body->setUserPointer(this);
    _entitiesMgr = EntitiesManager::getSingletonPtr();

    _animation = new AnimationBlender(_entity);
    _animation->blend("run", AnimationBlender::Switch, 0.5, true);
    _skeleton = _entity->getSkeleton();
    _skeleton->getBone("Head")->setManuallyControlled(true);
    if (goingRight) {
        _skeleton->getBone("Head")->yaw(Ogre::Degree(90), Ogre::Node::TS_LOCAL);
    }
    if (!goingRight) {
        _skeleton->getBone("Head")->yaw(Ogre::Degree(90), Ogre::Node::TS_LOCAL);
    }
}

void Roller::update (float delta) {
    _animation->addTime(delta);
    if (_iddleTime > 0) {
        _iddleTime -= delta;
    }
    else {
        shoot(delta);
        run();
    }
}

void Roller::shoot (float delta) {
    _timeSinceLastShoot += delta;
    if (_timeSinceLastShoot > Constants::TIMESHOOTROLLER) {
        _entitiesMgr->addDynamicEntity(new RollerBullet(_body->getWorldPosition(), _bulletsId));
        _bulletsId++;
        _timeSinceLastShoot = 0;

    }
}

void Roller::run () {
    if (_goingRight) {
        _body->setLinearVelocity(Ogre::Vector3(_speed, 0.0, 0.0));
        if (_directionChangesLeft > 0) {
            if (_body->getWorldPosition().x > 4.5) {
                _goingRight = false;
                _directionChangesLeft--;
                _skeleton->getBone("Head")->yaw(Ogre::Degree(180), Ogre::Node::TS_LOCAL);
            }
        }
        else if (_body->getWorldPosition().x > 6) {
            dissapear();
        }
    }
    else {
        _body->setLinearVelocity(Ogre::Vector3(-_speed, 0.0, 0.0));
        if (_directionChangesLeft > 0) {
            if (_body->getWorldPosition().x < -4.5) {
                _goingRight = true;
                _directionChangesLeft--;
                _skeleton->getBone("Head")->yaw(Ogre::Degree(180), Ogre::Node::TS_LOCAL);
            }
        }
        else if (_body->getWorldPosition().x < -6) {
            dissapear();
        }
    }
}

void Roller::dissapear () {
    _remove = true;
}

void Roller::onCollision (Entity *entity) {
    if (entity) {
        if (entity->getType() == Type::PLAYERBULLET) {
            reduceLife(Constants::REDUCELIFEENEMY);
            if(getLife()<=0){
                dissapear();
                PlayState::getSingletonPtr()->updatePoints(Constants::POINTSROLLER);
                ParticlesManager::getSingletonPtr()->createParticleSystem(_body->getWorldPosition(), Particles::ENEMYEXPLOSION);
            }
        }
    }
    if (entity->getType() == Type::GRENADEEXPLOSION) {
        PlayState::getSingletonPtr()->updatePoints(Constants::POINTSROLLER);
        ParticlesManager::getSingletonPtr()->createParticleSystem(_body->getWorldPosition(), Particles::ENEMYEXPLOSION);
        dissapear();
    }
}

int Roller::_bulletsId = 0;
