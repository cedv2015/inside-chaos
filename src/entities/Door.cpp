#include "Door.h"
#include "Constants.h"
#include "SoundFXManager.h"

Door::Door (int position) : DynamicEntity (nullptr, nullptr, nullptr, nullptr, Type::DOOR), _position(position), _animation(nullptr) {
    Ogre::Root *root = Ogre::Root::getSingletonPtr();
    Ogre::SceneManager *sceneMgr = root->getSceneManager("SceneManager");
    _entity = sceneMgr->createEntity("Door" + std::to_string(_position), "Big_door.mesh");
    _node = sceneMgr->createSceneNode("DoorNode" + std::to_string(_position));
    sceneMgr->getRootSceneNode()->addChild(_node);
    _node->attachObject(_entity);
    float real_position = - 100 + Constants::NUMBER_ELEMENTS_PER_SCENARIO * Constants::WIDTH_OF_MODULES * _position;

    _animation = new AnimationBlender(_entity);
    _node->setPosition(Ogre::Vector3(0, 0, real_position));
    _node->yaw(Ogre::Degree(180));
    SoundFXManager *soundFXMgr = SoundFXManager::getSingletonPtr();
    _door_open = soundFXMgr->load("door_open.wav");
    _door_close = soundFXMgr->load("door_close.wav");
}

void Door::update (float delta) {
    if (_animation) {
        _animation->addTime(delta);
    }
}

void Door::moveForward () {
    _node->translate(Ogre::Vector3(0, 0, Constants::NUMBER_ELEMENTS_PER_SCENARIO * Constants::WIDTH_OF_MODULES * 3));
}

void Door::open () {
    _animation->blend("open", AnimationBlender::Switch, 0.67, false);
    _door_open->play(0);
}

void Door::close () {
    _animation->blend("close", AnimationBlender::Switch, 0.41, false);
    _door_close->play(0);
}
