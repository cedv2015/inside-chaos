#include "Machinegun.h"
#include "EntitiesManager.h"

Machinegun::Machinegun (Ogre::Vector3 player_position) : Weapon(), _last_shot(0), _animation(nullptr) {
    _entitiesMgr = EntitiesManager::getSingletonPtr();
    Ogre::SceneManager *sceneMgr = Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager");
    _entity = sceneMgr->createEntity("Machinegun", "Machinegun.mesh");
    _node = sceneMgr->createSceneNode("Machinegun");
    sceneMgr->getRootSceneNode()->addChild(_node);
    _node->attachObject(_entity);
    _node->setPosition(player_position);
    _node->setVisible(false);
    _camera = sceneMgr->getCamera("MainCamera");
    SoundFXManager *soundFXMgr = SoundFXManager::getSingletonPtr();
    _sound_shoot = soundFXMgr->load("machinegun_shoot.wav");
    _animation = new AnimationBlender(_entity);
}

void Machinegun::shoot(bool last_clicked) {
    if (_last_shot <= 0) {
        PlayerBullet *shot = new PlayerBullet(_shots++);
        _entitiesMgr->addDynamicEntity(shot);
        _last_shot = 0.3f;
        _sound_shoot->play(0);
        _animation->blend("Shoot", AnimationBlender::Switch, 0.25, false);
    }
}

void Machinegun::update (float delta) {
    _node->setPosition(_camera->getPosition());
    _node->setOrientation(_camera->getOrientation());
    _node->translate(0, -0.3, -0.4, Ogre::Node::TS_LOCAL);
    _node->yaw(Ogre::Degree(180));
    if (_last_shot > 0) {
        _last_shot -= delta;
    }
    _animation->addTime(delta);
}

void Machinegun::swapVisibility() {
    _node->flipVisibility();
}

int Weapon::_shots = 0;
