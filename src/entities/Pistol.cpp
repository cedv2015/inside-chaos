#include "Pistol.h"
#include "EntitiesManager.h"

Pistol::Pistol (Ogre::Vector3 player_position) : Weapon() {
    _entitiesMgr = EntitiesManager::getSingletonPtr();
    Ogre::SceneManager *sceneMgr = Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager");
    _entity = sceneMgr->createEntity("Pistol", "Pistol.mesh");
    _node = sceneMgr->createSceneNode("PistolNode");
    sceneMgr->getRootSceneNode()->addChild(_node);
    _node->attachObject(_entity);
    _node->setPosition(player_position);
    _node->setVisible(false);
    _camera = sceneMgr->getCamera("MainCamera");
    SoundFXManager *soundFXMgr = SoundFXManager::getSingletonPtr();
    _sound_shoot = soundFXMgr->load("pistol_shoot.wav");
    _animation = new AnimationBlender(_entity);
}

void Pistol::shoot(bool last_clicked) {
    if (!last_clicked) {
        PlayerBullet *shot = new PlayerBullet(_shots++);
        _entitiesMgr->addDynamicEntity(shot);
        _sound_shoot->play(0);
        _animation->blend("Shoot", AnimationBlender::Switch, 0.25, false);
    }
}

void Pistol::update (float delta) {
    _node->setPosition(_camera->getPosition());
    _node->setOrientation(_camera->getOrientation());
    _node->translate(0, -0.3, -0.4, Ogre::Node::TS_LOCAL);
    _node->yaw(Ogre::Degree(180));
    _animation->addTime(delta);
}

void Pistol::swapVisibility() {
    _node->flipVisibility();
}
