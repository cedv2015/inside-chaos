#include "Grenade.h"
#include "PhysicsManager.h"
#include "Constants.h"
#include "ParticlesManager.h"
#include <Shapes/OgreBulletCollisionsSphereShape.h>

Grenade::Grenade () : DynamicEntity(nullptr, nullptr, nullptr, nullptr, Type::GRENADE), _time_alive(0), _exploded(false), _sceneMgr(nullptr) {
    Ogre::Root *root = Ogre::Root::getSingletonPtr();
    _sceneMgr = root->getSceneManager("SceneManager");
    _entity = _sceneMgr->createEntity("Grenade" + std::to_string(_id), "Grenade.mesh");
    _node = _sceneMgr->createSceneNode("GrenadeNode" + std::to_string(_id));
    _sceneMgr->getRootSceneNode()->addChild(_node);
    _node->attachObject(_entity);

    PhysicsManager *physicsMgr = PhysicsManager::getSingletonPtr();

    _shape = new OgreBulletCollisions::SphereCollisionShape(0.2);
    _body = new OgreBulletDynamics::RigidBody("rigidBodyGrenade" + std::to_string(_id), physicsMgr->getWorld(), Constants::GRENADE, Constants::grenade_collides_with);
    Ogre::Camera *camera = _sceneMgr->getCamera("MainCamera");
    Ogre::Vector3 position = (camera->getDerivedPosition() + camera->getDerivedDirection().normalisedCopy() * 2);
    _body->setShape(_node, _shape, 3, 0.8, 0.1, position);
    _body->setLinearVelocity(camera->getDerivedDirection().normalisedCopy() * 15.0f );
    auto bullet_body = _body->getBulletRigidBody();
    bullet_body->setUserPointer(this);
    ++_id;
    SoundFXManager *soundFXMgr = SoundFXManager::getSingletonPtr();
    _sound_explosion = soundFXMgr->load("grenade_explosion.wav");
}

void Grenade::update (float delta) {
    _time_alive += delta;
    if (_time_alive > Constants::GRENADETIME) {
        explode ();
    }
}

void Grenade::explode () {
    if (_exploded) {
        if (_time_alive > Constants::GRENADETIME + 0.1) {
            _remove = true;
        }
    }
    else {
        _exploded = true;
        Ogre::Vector3 position = _body->getWorldPosition();
        _node->setVisible(false);
        PhysicsManager *physicsMgr = PhysicsManager::getSingletonPtr();
        physicsMgr->addRigidBody(_body);
        physicsMgr->addCollisionShape(_shape);
        _body = new OgreBulletDynamics::RigidBody("rigidBodyGrenadeExplosion" + std::to_string(_id), physicsMgr->getWorld(), Constants::GRENADEEXPLOSION, Constants::grenadeExplosion_collides_with);
        _shape = new OgreBulletCollisions::SphereCollisionShape(3);
        ++_id;
        _body->setShape(_node, _shape, 0.1, 0.1, 100, position);
        auto bullet_body = _body->getBulletRigidBody();
        bullet_body->setUserPointer(this);
        bullet_body->setGravity(btVector3(0,0,0));
        bullet_body->setLinearFactor(btVector3(0, 0, 0));
        _type = Type::GRENADEEXPLOSION;
        _sound_explosion->play(0);
        ParticlesManager::getSingletonPtr()->createParticleSystem(_body->getWorldPosition(), Particles::GRENADEEXPLOSION);
    }
}

int Grenade::_id = 0;
