#include "GameOverState.h"

#define SENSITIVITY 0.2

template <> GameOverState* Ogre::Singleton<GameOverState>::msSingleton = nullptr;

GameOverState::GameOverState () : _root(nullptr), _sceneMgr(nullptr), _camera(nullptr), _keyboard(nullptr), _mouse(nullptr), _exit(false), _physicsMgr(nullptr), _entitiesMgr(nullptr), _rankingMgr(nullptr), _score(0), _ranking(nullptr), _exitButton(nullptr){
    _rankingMgr = RankingManager::getSingletonPtr();
}

GameOverState* GameOverState::getSingletonPtr () {
    return msSingleton;
}

GameOverState& GameOverState::getSingleton () {
    assert (msSingleton);
    return *msSingleton;
}

void GameOverState::enter () {

    std::cout << "Entered in GameOverState" << std::endl;
    _root = Ogre::Root::getSingletonPtr();
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("MainCamera");

    _physicsMgr = PhysicsManager::getSingletonPtr();
    _entitiesMgr = EntitiesManager::getSingletonPtr();
    PlayState::getSingletonPtr()->hideHud();

    CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().show( );

    if(_rankingMgr->checkRanking(getScore())){
        addRanking();
    }else{
      gameOver();
    }

}

void GameOverState::exit() {}

void GameOverState::pause () {}

void GameOverState::resume () {}

bool GameOverState::frameStarted (const Ogre::FrameEvent &evt) {
    Ogre::Real delta = evt.timeSinceLastFrame;
    _physicsMgr->update(delta);
    _entitiesMgr->update(delta);
    return !_exit;
}


bool GameOverState::frameEnded (const Ogre::FrameEvent &evt) {
    return !_exit;
}

void GameOverState::keyPressed(const OIS::KeyEvent& evt)
{
  CEGUI::System::getSingleton().getDefaultGUIContext().injectKeyDown(static_cast<CEGUI::Key::Scan>(evt.key));
  CEGUI::System::getSingleton().getDefaultGUIContext().injectChar(evt.text);

}

void GameOverState::keyReleased(const OIS::KeyEvent& evt)
{
  CEGUI::System::getSingleton().getDefaultGUIContext().injectKeyUp(static_cast<CEGUI::Key::Scan>(evt.key));

}

void GameOverState::mouseMoved (const OIS::MouseEvent &e) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMousePosition(e.state.X.abs, e.state.Y.abs);
}
void GameOverState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(convertMouseButton(id));
}
void GameOverState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(convertMouseButton(id));
}


CEGUI::MouseButton GameOverState::convertMouseButton(OIS::MouseButtonID id)
{
    CEGUI::MouseButton ceguiId;
    switch(id)
    {
        case OIS::MB_Left:
            ceguiId = CEGUI::LeftButton;
            break;
        case OIS::MB_Right:
            ceguiId = CEGUI::RightButton;
            break;
        case OIS::MB_Middle:
            ceguiId = CEGUI::MiddleButton;
            break;
        default:
            ceguiId = CEGUI::LeftButton;
    }
    return ceguiId;
}

void GameOverState::addRanking(){
    if(_ranking == NULL){
        _ranking = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow","Game/Overlay");
        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(_ranking);

        CEGUI::Window* overlay = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Overlay","Game/Panel");
        overlay->setSize(CEGUI::USize(CEGUI::UDim(0.5,0),CEGUI::UDim(0.4,0)));
        overlay->setPosition(CEGUI::UVector2(CEGUI::UDim(0.25,0),CEGUI::UDim(0.2,0)));

        CEGUI::Window* label = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label","Label");
        label->setSize(CEGUI::USize(CEGUI::UDim(0.3,0),CEGUI::UDim(0.3,0)));
        label->setPosition(CEGUI::UVector2(CEGUI::UDim(0.35,0),CEGUI::UDim(0.15,0)));
        label->setText("Put your name here...");


        CEGUI::Window* editBox = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Editbox","NameBox");
        editBox->setSize(CEGUI::USize(CEGUI::UDim(0.2,0),CEGUI::UDim(0.05,0)));
        editBox->setPosition(CEGUI::UVector2(CEGUI::UDim(0.4,0),CEGUI::UDim(0.35,0)));

        CEGUI::Window* exitButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/ButtonPause","ExitRecord");
        exitButton->setSize(CEGUI::USize(CEGUI::UDim(0.1,0),CEGUI::UDim(0.05,0)));
        exitButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.6,0),CEGUI::UDim(0.45,0)));
        exitButton->setText("CLOSE");
        exitButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&GameOverState::hide, this));

        CEGUI::Window* acceptButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/ButtonPause","AcceptRecord");
        acceptButton->setSize(CEGUI::USize(CEGUI::UDim(0.1,0),CEGUI::UDim(0.05,0)));
        acceptButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.3,0),CEGUI::UDim(0.45,0)));
        acceptButton->setText("ACCEPT");
        acceptButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&GameOverState::accept, this));
        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(overlay);
        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(label);
        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(editBox);
        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(exitButton);
        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(acceptButton);
        _ranking->addChild(overlay);
        _ranking->addChild(label);
        _ranking->addChild(editBox);
        _ranking->addChild(exitButton);
        _ranking->addChild(acceptButton);
        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(_ranking);
    }else{
        _ranking->show();
    }
}

bool GameOverState::hide(const CEGUI::EventArgs &e)
{
    _ranking->hide();
    this->popState();
    changeState(MenuState::getSingletonPtr());
    return true;

}

bool GameOverState::accept(const CEGUI::EventArgs &e)
{
    CEGUI::Window* name_record = _ranking->getChild("NameBox");
    std::string name = name_record->getText().c_str();
    name_record->setText("");
    _rankingMgr->setRanking(name, _score);
    _ranking->hide();
    this->popState();
    changeState(MenuState::getSingletonPtr());
    return true;

}
void GameOverState::gameOver(){
    if(_exitButton == NULL){
        _exitButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/ButtonPause","ExitGameOver");
        _exitButton->setText("EXIT");
        _exitButton->setSize(CEGUI::USize(CEGUI::UDim(0.2,0),CEGUI::UDim(0.05,0)));
        _exitButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.4,0),CEGUI::UDim(0.9,0)));
        _exitButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&GameOverState::close, this));
        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(_exitButton);
    }else{
        _exitButton->show();
    }
}

bool GameOverState::close(const CEGUI::EventArgs &e)
{
    _exitButton->hide();
    this->popState();
    changeState(MenuState::getSingletonPtr());
    return true;

}
