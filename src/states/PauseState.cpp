#include "PauseState.h"
#include "SoundFXManager.h"

template <> PauseState* Ogre::Singleton<PauseState>::msSingleton = nullptr;

PauseState::PauseState () :  _root(nullptr), _sceneMgr(nullptr), _camera(nullptr), _planePause(nullptr), _planePause_Node(nullptr), _keyboard(nullptr), _mouse(nullptr) ,_exit(false), _pauseMenu(nullptr), _particlesMgr(nullptr) {}

PauseState& PauseState::getSingleton () {
    assert (msSingleton);
    return *msSingleton;
}
PauseState* PauseState::getSingletonPtr () {
    return msSingleton;
}

void PauseState::enter () {
    std::cout << "Entered in PauseState" << std::endl;
    _root = Ogre::Root::getSingletonPtr();
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("MainCamera");
    _particlesMgr = ParticlesManager::getSingletonPtr();
    //_viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
    CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().show( );
    pauseCEGUI();
    wallpaper();
}

void PauseState::exit () {
    _planePause_Node->setVisible(false);
    _planePause_Node->getParent()->removeChild(_planePause_Node);
    _sceneMgr->destroySceneNode(_planePause_Node);
    _sceneMgr->destroyEntity(_planePause);
    _planePause_Node = nullptr;
    _planePause = nullptr;
}
void PauseState::pause() {}
void PauseState::resume() {}

void PauseState::keyPressed (const OIS::KeyEvent &e) {}
void PauseState::keyReleased (const OIS::KeyEvent &e) {
    switch (e.key) {
        case OIS::KC_P:
            //SoundFXManager::getSingletonPtr()->load("pause.wav")->play();
            CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().hide( );
            _pauseMenu->hide();
            popState();
            break;
        default:
            break;
    }
}

CEGUI::MouseButton PauseState::convertMouseButton(OIS::MouseButtonID id)
{
    CEGUI::MouseButton ceguiId;
    switch(id)
    {
        case OIS::MB_Left:
            ceguiId = CEGUI::LeftButton;
            break;
        case OIS::MB_Right:
            ceguiId = CEGUI::RightButton;
            break;
        case OIS::MB_Middle:
            ceguiId = CEGUI::MiddleButton;
            break;
        default:
            ceguiId = CEGUI::LeftButton;
    }
    return ceguiId;
}

void PauseState::mouseMoved (const OIS::MouseEvent &e) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMousePosition(e.state.X.abs, e.state.Y.abs);
}
void PauseState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(convertMouseButton(id));
}
void PauseState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(convertMouseButton(id));
}

bool PauseState::frameStarted (const Ogre::FrameEvent &e) {
    _particlesMgr->update(0);
    return !_exit;
}
bool PauseState::frameEnded (const Ogre::FrameEvent &e) {
    return !_exit;
}

void PauseState::wallpaper(){

    if(_planePause_Node == nullptr){
        _planePause = _sceneMgr->createEntity("PlanePause", "Plane.mesh");
        _planePause_Node= _sceneMgr->createSceneNode("PlanePause_Node");
        _sceneMgr->getRootSceneNode()->addChild(_planePause_Node);
        _planePause_Node->attachObject(_planePause);
        Ogre::Vector3 position = _sceneMgr->getCamera("MainCamera")->getPosition();
        _planePause_Node->setPosition(Ogre::Vector3(0, 0, position.z+0.5));
        _planePause->setMaterial(Ogre::MaterialManager::getSingleton().getByName("MaterialPause"));
    }else{
        _planePause_Node->setVisible(true);
    }
}
void PauseState::pauseCEGUI (){
if(_pauseMenu == NULL){

        _pauseMenu = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow","Game/SheetMenuPause");

        //Resume button
        CEGUI::Window* resumeButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/ButtonPause","Game/ResumeButton");
        resumeButton->setText("RESUME");
        resumeButton->setSize(CEGUI::USize(CEGUI::UDim(0.2,0),CEGUI::UDim(0.05,0)));
        resumeButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.4,0),CEGUI::UDim(0.2,0)));
        resumeButton->subscribeEvent(CEGUI::PushButton::EventClicked,
        CEGUI::Event::Subscriber(&PauseState::play, this));



        //Menu button
        CEGUI::Window* menuButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/ButtonPause","Game/MenuPauseButton");
        menuButton->setText("MENU");
        menuButton->setSize(CEGUI::USize(CEGUI::UDim(0.2,0),CEGUI::UDim(0.05,0)));
        menuButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.4,0),CEGUI::UDim(0.25,0)));
        menuButton->subscribeEvent(CEGUI::PushButton::EventClicked,
        CEGUI::Event::Subscriber(&PauseState::menu, this));

        //Quit button
        CEGUI::Window* quitButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/ButtonPause","Game/QuitPauseButton");
        quitButton->setText("EXIT");
        quitButton->setSize(CEGUI::USize(CEGUI::UDim(0.2,0),CEGUI::UDim(0.05,0)));
        quitButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.4,0),CEGUI::UDim(0.3,0)));
        quitButton->subscribeEvent(CEGUI::PushButton::EventClicked,
        CEGUI::Event::Subscriber(&PauseState::quit, this));

        //Attaching buttons
        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(resumeButton);
        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(menuButton);
        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(quitButton);
        _pauseMenu->addChild(resumeButton);
        _pauseMenu->addChild(menuButton);
        _pauseMenu->addChild(quitButton);
        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(_pauseMenu);

    }else{
        _pauseMenu->show();
    }
}

bool PauseState::quit(const CEGUI::EventArgs &e)
{
    _exit = true;
    return true;
}

bool PauseState::menu(const CEGUI::EventArgs &e)
{
    this->popState();
    _pauseMenu->hide();
    changeState(MenuState::getSingletonPtr());
    return true;
}

bool PauseState::play(const CEGUI::EventArgs &e)
{
    this->popState();
    _pauseMenu->hide();
    CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().hide( );
    return true;
}
