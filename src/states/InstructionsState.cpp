#include "InstructionsState.h"

template<> InstructionsState* Ogre::Singleton<InstructionsState>::msSingleton = nullptr;

InstructionsState::InstructionsState () : _root(nullptr),  _sceneMgr(nullptr), _camera(nullptr), _keyboard(nullptr), _mouse(nullptr), _exit(false), _instructions(nullptr){
}

InstructionsState* InstructionsState::getSingletonPtr () {
    return msSingleton;
}

InstructionsState& InstructionsState::getSingleton () {
    assert (msSingleton);
    return *msSingleton;
}

void InstructionsState::enter () {
  	_root = Ogre::Root::getSingletonPtr();
  	_sceneMgr = _root->getSceneManager("SceneManager");
  	_camera = _sceneMgr->getCamera("MainCamera");
  	_viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
    std::cout << "Entered in InstructionsState" << std::endl;
    wallpaper();
    instructionsShow();
}

void InstructionsState::exit () {
  	_sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
}

void InstructionsState::pause () {}

void InstructionsState::resume () {}

bool InstructionsState::frameStarted (const Ogre::FrameEvent &evt) {
    return !_exit;
}

bool InstructionsState::frameEnded (const Ogre::FrameEvent &evt) {
    return !_exit;
}

void InstructionsState::keyPressed (const OIS::KeyEvent &e) {
    switch (e.key) {
        default:
            break;
    }
}

void InstructionsState::keyReleased (const OIS::KeyEvent &e) {
    switch (e.key) {
        case OIS::KC_ESCAPE:
            _exit = true;
            break;
        case OIS::KC_P:
            changeState(PlayState::getSingletonPtr());
            break;
        default:
            break;
    }
}

void InstructionsState::mouseMoved (const OIS::MouseEvent &e) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMousePosition(e.state.X.abs, e.state.Y.abs);
}
void InstructionsState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(convertMouseButton(id));
}
void InstructionsState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(convertMouseButton(id));
}

CEGUI::MouseButton InstructionsState::convertMouseButton(OIS::MouseButtonID id)
{
  CEGUI::MouseButton ceguiId;
  switch(id)
    {
    case OIS::MB_Left:
      ceguiId = CEGUI::LeftButton;
      break;
    case OIS::MB_Right:
      ceguiId = CEGUI::RightButton;
      break;
    case OIS::MB_Middle:
      ceguiId = CEGUI::MiddleButton;
      break;
    default:
      ceguiId = CEGUI::LeftButton;
    }
  return ceguiId;
}

void InstructionsState::instructionsShow()
{
    if(_instructions == NULL){
        _instructions = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow","Game/SheetInstructions");


        CEGUI::Window* labelInstructions = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label","Game/CloseInstructionsButton");
        labelInstructions->setText("KEYBOARD + MOUSE");
        labelInstructions->setSize(CEGUI::USize(CEGUI::UDim(0.2,0),CEGUI::UDim(0.05,0)));
        labelInstructions->setPosition(CEGUI::UVector2(CEGUI::UDim(0.4,0),CEGUI::UDim(0.15,0)));

        CEGUI::Window* closeButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/ButtonPause","Game/CloseInstructionsButton");
        closeButton->setText("CLOSE");
        closeButton->setSize(CEGUI::USize(CEGUI::UDim(0.2,0),CEGUI::UDim(0.05,0)));
        closeButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.4,0),CEGUI::UDim(0.8,0)));
        closeButton->subscribeEvent(CEGUI::PushButton::EventClicked,
        CEGUI::Event::Subscriber(&InstructionsState::hide, this));
        //Attaching buttons
        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(closeButton);
        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(labelInstructions);
        _instructions->addChild(closeButton);
        _instructions->addChild(labelInstructions);
        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(_instructions);
    } else{
        _instructions->show();
    }

}

bool InstructionsState::hide(const CEGUI::EventArgs &e)
{
    _instructions->hide();
    changeState(MenuState::getSingletonPtr());
    return true;

}

void InstructionsState::wallpaper(){
    Ogre::TexturePtr m_backgroundTexture = Ogre::TextureManager::getSingleton().createManual("BackgroundInstructionsTexture",Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, Ogre::TEX_TYPE_2D,640, 480, 0, Ogre::PF_BYTE_BGR,Ogre::TU_DYNAMIC_WRITE_ONLY_DISCARDABLE);
    Ogre::Image m_backgroundImage;
    m_backgroundImage.load("wallpapers_instructions.jpg",Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
    m_backgroundTexture->loadImage(m_backgroundImage);
    Ogre::MaterialPtr material = Ogre::MaterialManager::getSingleton().create("BackgroundInstructionsMaterial", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
    material->getTechnique(0)->getPass(0)->createTextureUnitState("BackgroundInstructionsTexture");
    material->getTechnique(0)->getPass(0)->setLightingEnabled(false);
    material->getTechnique(0)->getPass(0)->getTextureUnitState(0)->setScrollAnimation(0.01, 0.0);
    Ogre::Rectangle2D* rect = new Ogre::Rectangle2D(true);
    rect->setCorners(-1.0, 1.0, 1.0, -1.0);
    rect->setMaterial("BackgroundInstructionsMaterial");
    rect->setRenderQueueGroup(Ogre::RENDER_QUEUE_BACKGROUND);
    rect->setBoundingBox(Ogre::AxisAlignedBox(-100000.0*Ogre::Vector3::UNIT_SCALE, 100000.0*Ogre::Vector3::UNIT_SCALE));
    Ogre::SceneNode* headNode = _sceneMgr->getRootSceneNode()->createChildSceneNode("BackgroundInstructions");
    headNode->attachObject(rect);

    Ogre::Rectangle2D *rectFog= new Ogre::Rectangle2D(true);
    rectFog->setCorners(-1.0, 1.0, 1.0, -1.0);
    rectFog->setBoundingBox(Ogre::AxisAlignedBox(-100000.0f * Ogre::Vector3::UNIT_SCALE, 100000.0f * Ogre::Vector3::UNIT_SCALE));
    Ogre::MaterialPtr materialFog = Ogre::MaterialManager::getSingleton().getByName("MaterialFog");
    materialFog->getTechnique(0)->getPass(0)->setDepthCheckEnabled(false);
    materialFog->getTechnique(0)->getPass(0)->setDepthWriteEnabled(false);
    materialFog->getTechnique(0)->getPass(0)->setLightingEnabled(false);
    rectFog->setMaterial("MaterialFog");
    materialFog->getTechnique(0)->getPass(0)->getTextureUnitState(0)->setScrollAnimation(0.01, 0.01);
    rectFog->setRenderQueueGroup(Ogre::RENDER_QUEUE_BACKGROUND);
    rectFog->setBoundingBox(Ogre::AxisAlignedBox(-100000.0*Ogre::Vector3::UNIT_SCALE, 100000.0*Ogre::Vector3::UNIT_SCALE));
    Ogre::SceneNode* instNode = _sceneMgr->getRootSceneNode()->createChildSceneNode("BackgroundFogInstructions");
    instNode->attachObject(rectFog);

    Ogre::Rectangle2D *rectRails = new Ogre::Rectangle2D(true);
    rectRails->setCorners(-1.0, 1.0, 1.0, -1.0);
    rectRails->setBoundingBox(Ogre::AxisAlignedBox(-100000.0f * Ogre::Vector3::UNIT_SCALE, 100000.0f * Ogre::Vector3::UNIT_SCALE));
    Ogre::MaterialPtr materialRails = Ogre::MaterialManager::getSingleton().getByName("MaterialInstructions");
    materialRails->getTechnique(0)->getPass(0)->setDepthCheckEnabled(false);
    materialRails->getTechnique(0)->getPass(0)->setDepthWriteEnabled(false);
    materialRails->getTechnique(0)->getPass(0)->setLightingEnabled(false);
    rectRails->setMaterial("MaterialInstructions");
    rectRails->setRenderQueueGroup(Ogre::RENDER_QUEUE_BACKGROUND);
    rectRails->setBoundingBox(Ogre::AxisAlignedBox(-100000.0*Ogre::Vector3::UNIT_SCALE, 100000.0*Ogre::Vector3::UNIT_SCALE));
    Ogre::SceneNode* railsNode = _sceneMgr->getRootSceneNode()->createChildSceneNode("BackgroundInstructionsKM");
    railsNode->attachObject(rectRails);

}
