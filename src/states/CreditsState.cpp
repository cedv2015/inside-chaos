#include "CreditsState.h"

template<> CreditsState* Ogre::Singleton<CreditsState>::msSingleton = nullptr;

CreditsState::CreditsState () : _root(nullptr),  _sceneMgr(nullptr), _camera(nullptr), _keyboard(nullptr), _mouse(nullptr), _exit(false), _credits(nullptr){
}

CreditsState* CreditsState::getSingletonPtr () {
    return msSingleton;
}

CreditsState& CreditsState::getSingleton () {
    assert (msSingleton);
    return *msSingleton;
}

void CreditsState::enter () {
  	_root = Ogre::Root::getSingletonPtr();
  	_sceneMgr = _root->getSceneManager("SceneManager");
  	_camera = _sceneMgr->getCamera("MainCamera");
  	_viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
    std::cout << "Entered in CreditsState" << std::endl;
    wallpaper();
    creditsShow();
}

void CreditsState::exit () {
  	_sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
}

void CreditsState::pause () {}

void CreditsState::resume () {}

bool CreditsState::frameStarted (const Ogre::FrameEvent &evt) {
    return !_exit;
}

bool CreditsState::frameEnded (const Ogre::FrameEvent &evt) {
    return !_exit;
}

void CreditsState::keyPressed (const OIS::KeyEvent &e) {
    switch (e.key) {
        default:
            break;
    }
}

void CreditsState::keyReleased (const OIS::KeyEvent &e) {
    switch (e.key) {
        case OIS::KC_ESCAPE:
            _exit = true;
            break;
        default:
            break;
    }
}

void CreditsState::mouseMoved (const OIS::MouseEvent &e) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMousePosition(e.state.X.abs, e.state.Y.abs);
}
void CreditsState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(convertMouseButton(id));
}
void CreditsState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(convertMouseButton(id));
}

CEGUI::MouseButton CreditsState::convertMouseButton(OIS::MouseButtonID id)
{
  CEGUI::MouseButton ceguiId;
  switch(id)
    {
    case OIS::MB_Left:
      ceguiId = CEGUI::LeftButton;
      break;
    case OIS::MB_Right:
      ceguiId = CEGUI::RightButton;
      break;
    case OIS::MB_Middle:
      ceguiId = CEGUI::MiddleButton;
      break;
    default:
      ceguiId = CEGUI::LeftButton;
    }
  return ceguiId;
}

void CreditsState::creditsShow()
{
    if(_credits == NULL){
        _credits = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow","Game/SheetCredits");

        CEGUI::Window* closeButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/ButtonPause","Game/CloseCreditsButton");
        closeButton->setText("CLOSE");
        closeButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
        closeButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.75,0),CEGUI::UDim(0.9,0)));
        closeButton->subscribeEvent(CEGUI::PushButton::EventClicked,
        CEGUI::Event::Subscriber(&CreditsState::hide, this));
        //Attaching buttons
        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(closeButton);
        _credits->addChild(closeButton);
        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(_credits);
    } else{
        _credits->show();
    }

}

bool CreditsState::hide(const CEGUI::EventArgs &e)
{
    _credits->hide();
    changeState(MenuState::getSingletonPtr());
    return true;

}

void CreditsState::wallpaper(){
    Ogre::TexturePtr m_backgroundTexture = Ogre::TextureManager::getSingleton().createManual("BackgroundCreditsTexture",Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, Ogre::TEX_TYPE_2D,640, 480, 0, Ogre::PF_BYTE_BGR,Ogre::TU_DYNAMIC_WRITE_ONLY_DISCARDABLE);
    Ogre::Image m_backgroundImage;
    m_backgroundImage.load("credits.jpg",Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
    m_backgroundTexture->loadImage(m_backgroundImage);
    Ogre::MaterialPtr material = Ogre::MaterialManager::getSingleton().create("BackgroundCreditsMaterial", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
    material->getTechnique(0)->getPass(0)->createTextureUnitState("BackgroundCreditsTexture");
    material->getTechnique(0)->getPass(0)->setLightingEnabled(false);
    Ogre::Rectangle2D* rect = new Ogre::Rectangle2D(true);
    rect->setCorners(-1.0, 1.0, 1.0, -1.0);
    rect->setMaterial("BackgroundCreditsMaterial");
    rect->setRenderQueueGroup(Ogre::RENDER_QUEUE_BACKGROUND);
    rect->setBoundingBox(Ogre::AxisAlignedBox(-100000.0*Ogre::Vector3::UNIT_SCALE, 100000.0*Ogre::Vector3::UNIT_SCALE));
    Ogre::SceneNode* headNode = _sceneMgr->getRootSceneNode()->createChildSceneNode("BackgroundCredits");
    headNode->attachObject(rect);

    Ogre::Rectangle2D *rectLetters= new Ogre::Rectangle2D(true);
    rectLetters->setCorners(-0.65, 1.0, 0.65, -1.0);
    rectLetters->setBoundingBox(Ogre::AxisAlignedBox(-100000.0f * Ogre::Vector3::UNIT_SCALE, 100000.0f * Ogre::Vector3::UNIT_SCALE));
    Ogre::MaterialPtr materialLetters = Ogre::MaterialManager::getSingleton().getByName("MaterialCredits");
    materialLetters->getTechnique(0)->getPass(0)->setDepthCheckEnabled(false);
    materialLetters->getTechnique(0)->getPass(0)->setDepthWriteEnabled(false);
    materialLetters->getTechnique(0)->getPass(0)->setLightingEnabled(false);
    rectLetters->setMaterial("MaterialCredits");
    materialLetters->getTechnique(0)->getPass(0)->getTextureUnitState(0)->setScrollAnimation(0.0, -0.05);
    rectLetters->setRenderQueueGroup(Ogre::RENDER_QUEUE_BACKGROUND);
    rectLetters->setBoundingBox(Ogre::AxisAlignedBox(-100000.0*Ogre::Vector3::UNIT_SCALE, 100000.0*Ogre::Vector3::UNIT_SCALE));
    Ogre::SceneNode* letterNode = _sceneMgr->getRootSceneNode()->createChildSceneNode("BackgroundLettersCredits");
    letterNode->attachObject(rectLetters);



    /*Ogre::Rectangle2D *rectFog= new Ogre::Rectangle2D(true);
    rectFog->setCorners(-1.0, 1.0, 1.0, -1.0);
    rectFog->setBoundingBox(Ogre::AxisAlignedBox(-100000.0f * Ogre::Vector3::UNIT_SCALE, 100000.0f * Ogre::Vector3::UNIT_SCALE));
    Ogre::MaterialPtr materialFog = Ogre::MaterialManager::getSingleton().getByName("MaterialFog");
    materialFog->getTechnique(0)->getPass(0)->setDepthCheckEnabled(false);
    materialFog->getTechnique(0)->getPass(0)->setDepthWriteEnabled(false);
    materialFog->getTechnique(0)->getPass(0)->setLightingEnabled(false);
    rectFog->setMaterial("MaterialFog");
    materialFog->getTechnique(0)->getPass(0)->getTextureUnitState(0)->setScrollAnimation(0.01, 0.01);
    rectFog->setRenderQueueGroup(Ogre::RENDER_QUEUE_BACKGROUND);
    rectFog->setBoundingBox(Ogre::AxisAlignedBox(-100000.0*Ogre::Vector3::UNIT_SCALE, 100000.0*Ogre::Vector3::UNIT_SCALE));
    Ogre::SceneNode* instNode = _sceneMgr->getRootSceneNode()->createChildSceneNode("BackgroundFogInstructions");
    instNode->attachObject(rectFog);*/

}
