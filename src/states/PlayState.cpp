#include "PlayState.h"

#define SENSITIVITY 0.2

template <> PlayState* Ogre::Singleton<PlayState>::msSingleton = nullptr;

PlayState::PlayState () : _root(nullptr), _sceneMgr(nullptr), _camera(nullptr), _keyboard(nullptr), _mouse(nullptr), _exit(false), _bloodVisible(false), _physicsMgr(nullptr), _entitiesMgr(nullptr), _scenarioMgr(nullptr), _cameraMgr(nullptr), _player(nullptr), _numGrenades(nullptr), _numEnemies(nullptr), _blood(nullptr), _hud(nullptr), _particlesMgr(nullptr){

    _root = Ogre::Root::getSingletonPtr();
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("MainCamera");
    _cameraMgr = CameraManager::getSingletonPtr();
}

PlayState* PlayState::getSingletonPtr () {
    return msSingleton;
}

PlayState& PlayState::getSingleton () {
    assert (msSingleton);
    return *msSingleton;
}

void PlayState::enter () {
    std::cout << "Entered in PlayState" << std::endl;
    CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().hide( );

    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);

    _physicsMgr = PhysicsManager::getSingletonPtr();
    _physicsMgr->init();
    _scenarioMgr = ScenarioManager::getSingletonPtr();
    _entitiesMgr = EntitiesManager::getSingletonPtr();
    _player = _entitiesMgr->createPlayer();
    // create the first scenario
    _scenarioMgr->getScene();

    // translate the camera to the initial position
    _camera->setPosition(Ogre::Vector3(0, 1.8, 0));
    _camera->lookAt(Ogre::Vector3(0, 1.8, 10));

    _cameraMgr->setPlayer(_player->getRigidBody());

    _particlesMgr = ParticlesManager::getSingletonPtr();
    hud();
}

void PlayState::exit() {
    _root->getAutoCreatedWindow()->removeAllViewports();
    _entitiesMgr->removeEntities();
    _particlesMgr->destroyAllParticles();
    _scenarioMgr->removeAllElements();
    _cameraMgr->clear();
    _sceneMgr->clearScene();
    _player = nullptr;
    _hud->hide();
}

void PlayState::pause () {}

void PlayState::resume () {}

bool PlayState::frameStarted (const Ogre::FrameEvent &evt) {
    Ogre::Real delta = evt.timeSinceLastFrame;
    _entitiesMgr->update(delta);
    _particlesMgr->update(delta);
    _physicsMgr->update(delta);
    updateHUD();
    if (_player) {
        _cameraMgr->update(0);
        _player->updateWeapon();

    }
    if(_player->getHurt()){
        _blood->setVisible(true);
        bloodShow();
    }
    return !_exit;
}

bool PlayState::frameEnded (const Ogre::FrameEvent &evt) {
    return !_exit;
}

void PlayState::keyPressed (const OIS::KeyEvent &e) {
    switch (e.key) {
        case OIS::KC_A:
            if (_player) {
                _player->pressLeft();
            }
            break;
        case OIS::KC_D:
            if (_player) {
                _player->pressRight();
            }
            break;
        case OIS::KC_SPACE:
            if (_player) {
                _player->pressUp();
            }
            break;
        case OIS::KC_W:
            if (_player) {
                _player->pressForward();
            }
            break;
        case OIS::KC_S:
            if (_player) {
                _player->pressBack();
            }
            break;
        case OIS::KC_F12:
            if (_physicsMgr) {
                _physicsMgr->switchDebug ();
            }
        default:
            break;
    }
}

void PlayState::keyReleased (const OIS::KeyEvent &e) {
    switch (e.key) {
        case OIS::KC_A:
            if (_player) {
                _player->releaseLeft();
            }
            break;
        case OIS::KC_D:
            if (_player) {
                _player->releaseRight();
            }
            break;
        case OIS::KC_ESCAPE:
            _exit = true;
            break;
        case OIS::KC_SPACE:
            if (_player) {
                _player->releaseUp();
            }
            break;
        case OIS::KC_W:
            if (_player) {
                _player->releaseForward();
            }
            break;
        case OIS::KC_S:
            if (_player) {
                _player->releaseBack();
            }
            break;
        case OIS::KC_G:
            if (_player) {
                _player->releaseGrenade();
            }
            break;
        case OIS::KC_Q:
            if (_player) {
                _player->swapWeapon();
            }
            break;
        case OIS::KC_P:
            _blood->setVisible(false);
            this->pushState (PauseState::getSingletonPtr());
            break;
        default:
            break;
    }
}

void PlayState::mouseMoved (const OIS::MouseEvent &e) {
    _camera->yaw(Ogre::Degree(e.state.X.rel * -0.1f * SENSITIVITY));
    _camera->pitch(Ogre::Degree(e.state.Y.rel * -0.1f * SENSITIVITY));
    Ogre::Radian pitch = _camera->getOrientation().getPitch();
    if (pitch > Ogre::Degree(90) || pitch < Ogre::Degree(-90)) {
        _camera->pitch(Ogre::Degree(- e.state.Y.rel * -0.1f * SENSITIVITY));
    }
    _player->addRotation(Ogre::Degree(e.state.X.rel * -0.1f * SENSITIVITY).valueRadians());
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMousePosition(e.state.X.abs, e.state.Y.abs);
    _cameraMgr->update(0);
    _player->updateWeapon();
}

void PlayState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    switch (id) {
        case OIS::MB_Left:
            _player->pressButtonLeft();
            break;
        default:
            break;
    }
}

void PlayState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    switch (id) {
        case OIS::MB_Left:
            _player->releaseButtonLeft();
            break;
        default:
            break;
    }
}

void PlayState::hud(){

    if(_hud == NULL){
        _hud = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow","Game/SheetHUD");

        _blood = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/blood","Game/blood");
        _blood->setSize(CEGUI::USize(CEGUI::UDim(1.0,0),CEGUI::UDim(1.0,0)));
        _blood->setPosition(CEGUI::UVector2(CEGUI::UDim(0,0),CEGUI::UDim(0,0)));
        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(_blood);
        _hud->addChild(_blood);
        _blood->setVisible(false);

        CEGUI::Window* hud = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/HUD","Game/HUD");
        hud->setSize(CEGUI::USize(CEGUI::UDim(1.0,0),CEGUI::UDim(1.0,0)));
        hud->setPosition(CEGUI::UVector2(CEGUI::UDim(0,0),CEGUI::UDim(0,0)));
        //Attaching buttons
        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(hud);
        _hud->addChild(hud);

        CEGUI::Window* grenade = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Grenade","Game/Grenade");
        grenade->setSize(CEGUI::USize(CEGUI::UDim(0.08,0),CEGUI::UDim(0.1,0)));
        grenade->setPosition(CEGUI::UVector2(CEGUI::UDim(0.03,0),CEGUI::UDim(0.855,0)));
        //Attaching buttons
        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(grenade);
        _hud->addChild(grenade);

        _numGrenades = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label","Game/numGrenades");
        _numGrenades->setText("[colour='FF58AABF']"+std::to_string(_player->getGrenadesLeft()));
        _numGrenades->setSize(CEGUI::USize(CEGUI::UDim(0.1,0),CEGUI::UDim(0.05,0)));
        _numGrenades->setPosition(CEGUI::UVector2(CEGUI::UDim(0.04,0),CEGUI::UDim(0.88,0)));
        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(_numGrenades);
        _hud->addChild(_numGrenades);

        CEGUI::Window* enemy = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Enemy","Game/Enemy");
        enemy->setSize(CEGUI::USize(CEGUI::UDim(0.08,0),CEGUI::UDim(0.1,0)));
        enemy->setPosition(CEGUI::UVector2(CEGUI::UDim(0.9,0),CEGUI::UDim(0.825,0)));
        //Attaching buttons
        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(enemy);
        _hud->addChild(enemy);

        _numEnemies = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label","Game/numEnemies");
        _numEnemies->setText("[colour='FF58AABF']"+std::to_string(_player->getPoints()));
        _numEnemies->setSize(CEGUI::USize(CEGUI::UDim(0.1,0),CEGUI::UDim(0.05,0)));
        _numEnemies->setPosition(CEGUI::UVector2(CEGUI::UDim(0.89,0),CEGUI::UDim(0.92,0)));
        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(_numEnemies);
        _hud->addChild(_numEnemies);

        CEGUI::Window* _life0 = _hud->createChild("TaharezLook/Life0","Life0");
        _life0->setSize(CEGUI::USize(CEGUI::UDim(0.3,0),CEGUI::UDim(0.05,0)));
        _life0->setPosition(CEGUI::UVector2(CEGUI::UDim(0.35,0),CEGUI::UDim(0.005,0)));

        CEGUI::Window* _life1 = _hud->createChild("TaharezLook/Life1","Life1");
        _life1->setSize(CEGUI::USize(CEGUI::UDim(0.3,0),CEGUI::UDim(0.05,0)));
        _life1->setPosition(CEGUI::UVector2(CEGUI::UDim(0.35,0),CEGUI::UDim(0.005,0)));

        CEGUI::Window* _life2 = _hud->createChild("TaharezLook/Life2","Life2");
        _life2->setSize(CEGUI::USize(CEGUI::UDim(0.3,0),CEGUI::UDim(0.05,0)));
        _life2->setPosition(CEGUI::UVector2(CEGUI::UDim(0.35,0),CEGUI::UDim(0.005,0)));


        CEGUI::Window* _life3 = _hud->createChild("TaharezLook/Life3","Life3");
        _life3->setSize(CEGUI::USize(CEGUI::UDim(0.3,0),CEGUI::UDim(0.05,0)));
        _life3->setPosition(CEGUI::UVector2(CEGUI::UDim(0.35,0),CEGUI::UDim(0.005,0)));


        CEGUI::Window* _life4 = _hud->createChild("TaharezLook/Life4","Life4");
        _life4->setSize(CEGUI::USize(CEGUI::UDim(0.3,0),CEGUI::UDim(0.05,0)));
        _life4->setPosition(CEGUI::UVector2(CEGUI::UDim(0.35,0),CEGUI::UDim(0.005,0)));


        CEGUI::Window* _life5 = _hud->createChild("TaharezLook/Life5","Life5");
        _life5->setSize(CEGUI::USize(CEGUI::UDim(0.3,0),CEGUI::UDim(0.05,0)));
        _life5->setPosition(CEGUI::UVector2(CEGUI::UDim(0.35,0),CEGUI::UDim(0.005,0)));

        CEGUI::Window* _life6 = _hud->createChild("TaharezLook/Life6","Life6");
        _life6->setSize(CEGUI::USize(CEGUI::UDim(0.3,0),CEGUI::UDim(0.05,0)));
        _life6->setPosition(CEGUI::UVector2(CEGUI::UDim(0.35,0),CEGUI::UDim(0.005,0)));


        CEGUI::Window* _life7 = _hud->createChild("TaharezLook/Life7","Life7");
        _life7->setSize(CEGUI::USize(CEGUI::UDim(0.3,0),CEGUI::UDim(0.05,0)));
        _life7->setPosition(CEGUI::UVector2(CEGUI::UDim(0.35,0),CEGUI::UDim(0.005,0)));


        CEGUI::Window* _life8 = _hud->createChild("TaharezLook/Life8","Life8");
        _life8->setSize(CEGUI::USize(CEGUI::UDim(0.3,0),CEGUI::UDim(0.05,0)));
        _life8->setPosition(CEGUI::UVector2(CEGUI::UDim(0.35,0),CEGUI::UDim(0.005,0)));


        CEGUI::Window* _life9 = _hud->createChild("TaharezLook/Life9","Life9");
        _life9->setSize(CEGUI::USize(CEGUI::UDim(0.3,0),CEGUI::UDim(0.05,0)));
        _life9->setPosition(CEGUI::UVector2(CEGUI::UDim(0.35,0),CEGUI::UDim(0.005,0)));


        CEGUI::Window* _life10 = _hud->createChild("TaharezLook/Life10","Life10");
        _life10->setSize(CEGUI::USize(CEGUI::UDim(0.3,0),CEGUI::UDim(0.05,0)));
        _life10->setPosition(CEGUI::UVector2(CEGUI::UDim(0.35,0),CEGUI::UDim(0.005,0)));

        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(_hud);
    } else{
        _hud->show();
        _blood->setAlpha(0.0);
    }

}

void PlayState::updateHUD(){
    _numGrenades->setText("[colour='FF58AABF']"+std::to_string(_player->getGrenadesLeft()));
    _numEnemies->setText("[colour='FF58AABF']"+std::to_string(_player->getPoints()));

    int life = _player->getLife();
    if(life<=0){
        GameOverState::getSingletonPtr()->setScore(getPoints());
        pushState(GameOverState::getSingletonPtr());
    }
    _hud->getChild("Life"+std::to_string(life/20))->show();
    for(int i=1; i<=10; i++){
        if(i!=life/20)
            _hud->getChild("Life"+std::to_string(i))->hide();
    }
}

void PlayState::bloodShow(){
    float alpha = _blood->getAlpha();
    _blood->setAlpha(alpha-0.003);
    if(alpha<=0){
        _blood->setVisible(false);
        _blood->setAlpha(1.0);
        _player->setHurt(false);
    }else
        _blood->setAlpha(alpha-0.003);

}

void PlayState::hideHud(){
    _hud->hide();
    _blood->setAlpha(0.0);
    _player->hideWeapon();
}

void PlayState::updatePoints(int p){
    _player->updatePoints(p);
}

int PlayState::getPoints(){
    return _player->getPoints();
}
