#include "RankingState.h"

template<> RankingState* Ogre::Singleton<RankingState>::msSingleton = nullptr;

RankingState::RankingState () :_root(nullptr),  _sceneMgr(nullptr), _camera(nullptr), _rankingManager(nullptr), _keyboard(nullptr), _mouse(nullptr), _exit(false), _records(nullptr), _time(0) {

	_rankingManager = RankingManager::getSingletonPtr();

}

RankingState* RankingState::getSingletonPtr () {
    return msSingleton;
}

RankingState& RankingState::getSingleton () {
    assert (msSingleton);
    return *msSingleton;
}

void RankingState::enter () {
	_root = Ogre::Root::getSingletonPtr();
	_sceneMgr = _root->getSceneManager("SceneManager");
	_camera = _sceneMgr->getCamera("MainCamera");
	_viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
  std::cout << "Entered in RankingState" << std::endl;
  wallpaper();
  rankingShow();
}

void RankingState::exit () {
	  _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
}

void RankingState::pause () {}

void RankingState::resume () {}

bool RankingState::frameStarted (const Ogre::FrameEvent &evt) {
    return !_exit;
}

bool RankingState::frameEnded (const Ogre::FrameEvent &evt) {
    return !_exit;
}

void RankingState::keyPressed (const OIS::KeyEvent &e) {
    switch (e.key) {
        default:
            break;
    }
}

void RankingState::keyReleased (const OIS::KeyEvent &e) {
    switch (e.key) {
        case OIS::KC_ESCAPE:
            _exit = true;
            break;
        case OIS::KC_P:
            changeState(PlayState::getSingletonPtr());
            break;
        default:
            break;
    }
}

void RankingState::mouseMoved (const OIS::MouseEvent &e) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMousePosition(e.state.X.abs, e.state.Y.abs);
}
void RankingState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(convertMouseButton(id));
}
void RankingState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(convertMouseButton(id));
}

CEGUI::MouseButton RankingState::convertMouseButton(OIS::MouseButtonID id)
{
  CEGUI::MouseButton ceguiId;
  switch(id)
    {
    case OIS::MB_Left:
      ceguiId = CEGUI::LeftButton;
      break;
    case OIS::MB_Right:
      ceguiId = CEGUI::RightButton;
      break;
    case OIS::MB_Middle:
      ceguiId = CEGUI::MiddleButton;
      break;
    default:
      ceguiId = CEGUI::LeftButton;
    }
  return ceguiId;
}

void RankingState::rankingShow()
{
    if(_records == NULL){
      	_records = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow","Game/SheetRecords");

        CEGUI::Window* labelHighScore = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label","Game/HighScoresLabel");
        labelHighScore->setText("HIGH SCORES");
        labelHighScore->setSize(CEGUI::USize(CEGUI::UDim(0.2,0),CEGUI::UDim(0.05,0)));
        labelHighScore->setPosition(CEGUI::UVector2(CEGUI::UDim(0.4,0),CEGUI::UDim(0.05,0)));

        CEGUI::Window* closeButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/ButtonPause","Game/CloseRankingButton");
        closeButton->setText("CLOSE");
        closeButton->setSize(CEGUI::USize(CEGUI::UDim(0.2,0),CEGUI::UDim(0.05,0)));
        closeButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.4,0),CEGUI::UDim(0.9,0)));
        closeButton->subscribeEvent(CEGUI::PushButton::EventClicked,CEGUI::Event::Subscriber(&RankingState::hide, this));

				CEGUI::Window* labelName1 = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Ranking","Name1");
				labelName1->setSize(CEGUI::USize(CEGUI::UDim(0.7,0),CEGUI::UDim(0.05,0)));
				labelName1->setPosition(CEGUI::UVector2(CEGUI::UDim(0.15,0),CEGUI::UDim(0.15,0)));

				CEGUI::Window* labelpos1 = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label","Game/labelpos1");
				labelpos1->setText("[colour='FF555555']1");
				labelpos1->setSize(CEGUI::USize(CEGUI::UDim(0.2,0),CEGUI::UDim(0.05,0)));
				labelpos1->setPosition(CEGUI::UVector2(CEGUI::UDim(0.1,0),CEGUI::UDim(0.15,0)));

				CEGUI::Window* labelpoints1 = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label","Score1");
				labelpoints1->setSize(CEGUI::USize(CEGUI::UDim(0.2,0),CEGUI::UDim(0.05,0)));
				labelpoints1->setPosition(CEGUI::UVector2(CEGUI::UDim(0.7,0),CEGUI::UDim(0.15,0)));

				CEGUI::Window* labelName2 = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Ranking","Name2");
				labelName2->setSize(CEGUI::USize(CEGUI::UDim(0.7,0),CEGUI::UDim(0.05,0)));
				labelName2->setPosition(CEGUI::UVector2(CEGUI::UDim(0.15,0),CEGUI::UDim(0.225,0)));

				CEGUI::Window* labelpos2 = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label","Game/labelpos2");
				labelpos2->setText("[colour='FF555555']2");
				labelpos2->setSize(CEGUI::USize(CEGUI::UDim(0.2,0),CEGUI::UDim(0.05,0)));
				labelpos2->setPosition(CEGUI::UVector2(CEGUI::UDim(0.1,0),CEGUI::UDim(0.225,0)));

				CEGUI::Window* labelpoints2 = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label","Score2");
				labelpoints2->setSize(CEGUI::USize(CEGUI::UDim(0.2,0),CEGUI::UDim(0.05,0)));
				labelpoints2->setPosition(CEGUI::UVector2(CEGUI::UDim(0.7,0),CEGUI::UDim(0.225,0)));

				CEGUI::Window* labelName3 = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Ranking","Name3");
				labelName3->setSize(CEGUI::USize(CEGUI::UDim(0.7,0),CEGUI::UDim(0.05,0)));
				labelName3->setPosition(CEGUI::UVector2(CEGUI::UDim(0.15,0),CEGUI::UDim(0.3,0)));

				CEGUI::Window* labelpos3 = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label","Game/labelpos3");
				labelpos3->setText("[colour='FF555555']3");
				labelpos3->setSize(CEGUI::USize(CEGUI::UDim(0.2,0),CEGUI::UDim(0.05,0)));
				labelpos3->setPosition(CEGUI::UVector2(CEGUI::UDim(0.1,0),CEGUI::UDim(0.3,0)));

				CEGUI::Window* labelpoints3 = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label","Score3");
				labelpoints3->setSize(CEGUI::USize(CEGUI::UDim(0.2,0),CEGUI::UDim(0.05,0)));
				labelpoints3->setPosition(CEGUI::UVector2(CEGUI::UDim(0.7,0),CEGUI::UDim(0.3,0)));

				CEGUI::Window* labelName4 = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Ranking","Name4");
				labelName4->setSize(CEGUI::USize(CEGUI::UDim(0.7,0),CEGUI::UDim(0.05,0)));
				labelName4->setPosition(CEGUI::UVector2(CEGUI::UDim(0.15,0),CEGUI::UDim(0.375,0)));

				CEGUI::Window* labelpos4 = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label","Game/labelpos4");
				labelpos4->setText("[colour='FF555555']4");
				labelpos4->setSize(CEGUI::USize(CEGUI::UDim(0.2,0),CEGUI::UDim(0.05,0)));
				labelpos4->setPosition(CEGUI::UVector2(CEGUI::UDim(0.1,0),CEGUI::UDim(0.375,0)));

				CEGUI::Window* labelpoints4 = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label","Score4");
				labelpoints4->setSize(CEGUI::USize(CEGUI::UDim(0.2,0),CEGUI::UDim(0.05,0)));
				labelpoints4->setPosition(CEGUI::UVector2(CEGUI::UDim(0.7,0),CEGUI::UDim(0.375,0)));

				CEGUI::Window* labelName5 = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Ranking","Name5");
				labelName5->setSize(CEGUI::USize(CEGUI::UDim(0.7,0),CEGUI::UDim(0.05,0)));
				labelName5->setPosition(CEGUI::UVector2(CEGUI::UDim(0.15,0),CEGUI::UDim(0.45,0)));

				CEGUI::Window* labelpos5 = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label","Game/labelpos5");
				labelpos5->setText("[colour='FF555555']5");
				labelpos5->setSize(CEGUI::USize(CEGUI::UDim(0.2,0),CEGUI::UDim(0.05,0)));
				labelpos5->setPosition(CEGUI::UVector2(CEGUI::UDim(0.1,0),CEGUI::UDim(0.45,0)));

				CEGUI::Window* labelpoints5 = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label","Score5");
				labelpoints5->setSize(CEGUI::USize(CEGUI::UDim(0.2,0),CEGUI::UDim(0.05,0)));
				labelpoints5->setPosition(CEGUI::UVector2(CEGUI::UDim(0.7,0),CEGUI::UDim(0.45,0)));

				CEGUI::Window* labelName6 = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Ranking","Name6");
				labelName6->setSize(CEGUI::USize(CEGUI::UDim(0.7,0),CEGUI::UDim(0.05,0)));
				labelName6->setPosition(CEGUI::UVector2(CEGUI::UDim(0.15,0),CEGUI::UDim(0.525,0)));

				CEGUI::Window* labelpos6 = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label","Game/labelpos6");
				labelpos6->setText("[colour='FF555555']6");
				labelpos6->setSize(CEGUI::USize(CEGUI::UDim(0.2,0),CEGUI::UDim(0.05,0)));
				labelpos6->setPosition(CEGUI::UVector2(CEGUI::UDim(0.1,0),CEGUI::UDim(0.525,0)));

				CEGUI::Window* labelpoints6 = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label","Score6");
				labelpoints6->setSize(CEGUI::USize(CEGUI::UDim(0.2,0),CEGUI::UDim(0.05,0)));
				labelpoints6->setPosition(CEGUI::UVector2(CEGUI::UDim(0.7,0),CEGUI::UDim(0.525,0)));

				CEGUI::Window* labelName7 = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Ranking","Name7");
				labelName7->setSize(CEGUI::USize(CEGUI::UDim(0.7,0),CEGUI::UDim(0.05,0)));
				labelName7->setPosition(CEGUI::UVector2(CEGUI::UDim(0.15,0),CEGUI::UDim(0.6,0)));

				CEGUI::Window* labelpos7 = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label","Game/labelpos7");
				labelpos7->setText("[colour='FF555555']7");
				labelpos7->setSize(CEGUI::USize(CEGUI::UDim(0.2,0),CEGUI::UDim(0.05,0)));
				labelpos7->setPosition(CEGUI::UVector2(CEGUI::UDim(0.1,0),CEGUI::UDim(0.6,0)));

				CEGUI::Window* labelpoints7 = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label","Score7");
				labelpoints7->setSize(CEGUI::USize(CEGUI::UDim(0.2,0),CEGUI::UDim(0.05,0)));
				labelpoints7->setPosition(CEGUI::UVector2(CEGUI::UDim(0.7,0),CEGUI::UDim(0.6,0)));

				CEGUI::Window* labelName8 = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Ranking","Name8");
				labelName8->setSize(CEGUI::USize(CEGUI::UDim(0.7,0),CEGUI::UDim(0.05,0)));
				labelName8->setPosition(CEGUI::UVector2(CEGUI::UDim(0.15,0),CEGUI::UDim(0.675,0)));

				CEGUI::Window* labelpos8 = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label","Game/labelpos8");
				labelpos8->setText("[colour='FF555555']8");
				labelpos8->setSize(CEGUI::USize(CEGUI::UDim(0.2,0),CEGUI::UDim(0.05,0)));
				labelpos8->setPosition(CEGUI::UVector2(CEGUI::UDim(0.1,0),CEGUI::UDim(0.675,0)));

				CEGUI::Window* labelpoints8 = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label","Score8");
				labelpoints8->setSize(CEGUI::USize(CEGUI::UDim(0.2,0),CEGUI::UDim(0.05,0)));
				labelpoints8->setPosition(CEGUI::UVector2(CEGUI::UDim(0.7,0),CEGUI::UDim(0.675,0)));

				CEGUI::Window* labelName9 = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Ranking","Name9");
				labelName9->setSize(CEGUI::USize(CEGUI::UDim(0.7,0),CEGUI::UDim(0.05,0)));
				labelName9->setPosition(CEGUI::UVector2(CEGUI::UDim(0.15,0),CEGUI::UDim(0.75,0)));

				CEGUI::Window* labelpos9 = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label","Game/labelpos9");
				labelpos9->setText("[colour='FF555555']9");
				labelpos9->setSize(CEGUI::USize(CEGUI::UDim(0.2,0),CEGUI::UDim(0.05,0)));
				labelpos9->setPosition(CEGUI::UVector2(CEGUI::UDim(0.1,0),CEGUI::UDim(0.75,0)));

				CEGUI::Window* labelpoints9 = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label","Score9");
				labelpoints9->setSize(CEGUI::USize(CEGUI::UDim(0.2,0),CEGUI::UDim(0.05,0)));
				labelpoints9->setPosition(CEGUI::UVector2(CEGUI::UDim(0.7,0),CEGUI::UDim(0.75,0)));

				CEGUI::Window* labelName10 = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Ranking","Name10");
				labelName10->setSize(CEGUI::USize(CEGUI::UDim(0.7,0),CEGUI::UDim(0.05,0)));
				labelName10->setPosition(CEGUI::UVector2(CEGUI::UDim(0.15,0),CEGUI::UDim(0.825,0)));

				CEGUI::Window* labelpos10 = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label","Game/labelpos10");
				labelpos10->setText("[colour='FF555555']10");
				labelpos10->setSize(CEGUI::USize(CEGUI::UDim(0.2,0),CEGUI::UDim(0.05,0)));
				labelpos10->setPosition(CEGUI::UVector2(CEGUI::UDim(0.1,0),CEGUI::UDim(0.825,0)));

				CEGUI::Window* labelpoints10 = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label","Score10");
				labelpoints10->setSize(CEGUI::USize(CEGUI::UDim(0.2,0),CEGUI::UDim(0.05,0)));
				labelpoints10->setPosition(CEGUI::UVector2(CEGUI::UDim(0.7,0),CEGUI::UDim(0.825,0)));
        //Attaching buttons
				CEGUI::Window *root = CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow();
        root->addChild(closeButton);
        root->addChild(labelHighScore);
        root->addChild(labelName1);
        root->addChild(labelName2);
        root->addChild(labelName3);
        root->addChild(labelName4);
        root->addChild(labelName5);
        root->addChild(labelName6);
        root->addChild(labelName7);
        root->addChild(labelName8);
        root->addChild(labelName9);
        root->addChild(labelName10);
        root->addChild(labelpos1);
        root->addChild(labelpos2);
        root->addChild(labelpos3);
        root->addChild(labelpos4);
        root->addChild(labelpos5);
        root->addChild(labelpos6);
        root->addChild(labelpos7);
        root->addChild(labelpos8);
        root->addChild(labelpos9);
        root->addChild(labelpos10);
        root->addChild(labelpoints1);
        root->addChild(labelpoints2);
        root->addChild(labelpoints3);
        root->addChild(labelpoints4);
        root->addChild(labelpoints5);
        root->addChild(labelpoints6);
        root->addChild(labelpoints7);
        root->addChild(labelpoints8);
        root->addChild(labelpoints9);
        root->addChild(labelpoints10);
        _records->addChild(closeButton);
        _records->addChild(labelHighScore);
        _records->addChild(labelName1);
        _records->addChild(labelName2);
        _records->addChild(labelName3);
        _records->addChild(labelName4);
        _records->addChild(labelName5);
        _records->addChild(labelName6);
        _records->addChild(labelName7);
        _records->addChild(labelName8);
        _records->addChild(labelName9);
        _records->addChild(labelName10);
        _records->addChild(labelpos1);
        _records->addChild(labelpos2);
        _records->addChild(labelpos3);
        _records->addChild(labelpos4);
        _records->addChild(labelpos5);
        _records->addChild(labelpos6);
        _records->addChild(labelpos7);
        _records->addChild(labelpos8);
        _records->addChild(labelpos9);
        _records->addChild(labelpos10);
        _records->addChild(labelpoints1);
        _records->addChild(labelpoints2);
        _records->addChild(labelpoints3);
        _records->addChild(labelpoints4);
        _records->addChild(labelpoints5);
        _records->addChild(labelpoints6);
        _records->addChild(labelpoints7);
        _records->addChild(labelpoints8);
        _records->addChild(labelpoints9);
        _records->addChild(labelpoints10);
        root->addChild(_records);

	      std::vector<std::string> ranking = _rankingManager->getRanking();
        std::string name, score;
        for (unsigned int i=0; i<ranking.size(); i+=2){
            name = std::string("Name") + std::to_string(i/2+1);
            score = std::string("Score") + std::to_string(i/2+1);
            CEGUI::Window* name_record = _records->getChild(name);
            name_record->setText(ranking[i]);
            CEGUI::Window* score_record = _records->getChild(score);
            score_record ->setText(ranking[i+1]);
        }

    }else{
        _records->show();
				std::vector<std::string> ranking = _rankingManager->getRanking();
        std::string name, score;
        for (unsigned int i=0; i<ranking.size(); i+=2){
            name = std::string("Name") + std::to_string(i/2+1);
            score = std::string("Score") + std::to_string(i/2+1);
            CEGUI::Window* name_record = _records->getChild(name);
            name_record->setText(ranking[i]);
            CEGUI::Window* score_record = _records->getChild(score);
            score_record ->setText(ranking[i+1]);
        }

    }

}

bool RankingState::hide(const CEGUI::EventArgs &e)
{
    _records->hide();
    changeState(MenuState::getSingletonPtr());
    return true;

}

void RankingState::wallpaper(){

  Ogre::TexturePtr m_backgroundTexture = Ogre::TextureManager::getSingleton().createManual("BackgroundRankingTexture",Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, Ogre::TEX_TYPE_2D,640, 480, 0, Ogre::PF_BYTE_BGR,Ogre::TU_DYNAMIC_WRITE_ONLY_DISCARDABLE);
  Ogre::Image m_backgroundImage;
  m_backgroundImage.load("credits.jpg",Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
  m_backgroundTexture->loadImage(m_backgroundImage);
  Ogre::MaterialPtr material = Ogre::MaterialManager::getSingleton().create("BackgroundRankingMaterial", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
  material->getTechnique(0)->getPass(0)->createTextureUnitState("BackgroundRankingTexture");
  material->getTechnique(0)->getPass(0)->setLightingEnabled(false);
  Ogre::Rectangle2D* rect = new Ogre::Rectangle2D(true);
  rect->setCorners(-1.0, 1.0, 1.0, -1.0);
  rect->setMaterial("BackgroundRankingMaterial");
  rect->setRenderQueueGroup(Ogre::RENDER_QUEUE_BACKGROUND);
  rect->setBoundingBox(Ogre::AxisAlignedBox(-100000.0*Ogre::Vector3::UNIT_SCALE, 100000.0*Ogre::Vector3::UNIT_SCALE));
  Ogre::SceneNode* headNode = _sceneMgr->getRootSceneNode()->createChildSceneNode("BackgroundRanking");
  headNode->attachObject(rect);

  Ogre::Rectangle2D *highScore= new Ogre::Rectangle2D(true);
  highScore->setCorners(-0.75, 0.75, 0.75, -0.8);
  highScore->setBoundingBox(Ogre::AxisAlignedBox(-100000.0f * Ogre::Vector3::UNIT_SCALE, 100000.0f * Ogre::Vector3::UNIT_SCALE));
  Ogre::MaterialPtr materialPause = Ogre::MaterialManager::getSingleton().getByName("MaterialRanking");
  materialPause->getTechnique(0)->getPass(0)->setDepthCheckEnabled(false);
  materialPause->getTechnique(0)->getPass(0)->setDepthWriteEnabled(false);
  materialPause->getTechnique(0)->getPass(0)->setLightingEnabled(false);
  highScore->setMaterial("MaterialRanking");
  highScore->setRenderQueueGroup(Ogre::RENDER_QUEUE_BACKGROUND);
  highScore->setBoundingBox(Ogre::AxisAlignedBox(-100000.0*Ogre::Vector3::UNIT_SCALE, 100000.0*Ogre::Vector3::UNIT_SCALE));
  Ogre::SceneNode* rankingNode = _sceneMgr->getRootSceneNode()->createChildSceneNode("BackgroundBlackRanking");
  rankingNode->attachObject(highScore);
}
