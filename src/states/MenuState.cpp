#include "MenuState.h"

template<> MenuState* Ogre::Singleton<MenuState>::msSingleton = nullptr;

MenuState::MenuState () :  _root(nullptr), _sceneMgr(nullptr), _camera(nullptr), _backNode(nullptr), _fogNode(nullptr),_rect(nullptr), _rectFog(nullptr), _keyboard(nullptr), _mouse(nullptr), _exit(false), _sheet(nullptr), _instructions(nullptr), _time(0) {

}

MenuState* MenuState::getSingletonPtr () {
    return msSingleton;
}

MenuState& MenuState::getSingleton () {
    assert (msSingleton);
    return *msSingleton;
}

void MenuState::enter () {
    std::cout << "Entered in MenuState" << std::endl;
    _root = Ogre::Root::getSingletonPtr();
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("MainCamera");
    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
    menu();
    createGUI();
}

void MenuState::exit () {
    _sceneMgr->clearScene();
    _sheet->hide();
    _root->getAutoCreatedWindow()->removeAllViewports();
}

void MenuState::pause () {}

void MenuState::resume () {}

bool MenuState::frameStarted (const Ogre::FrameEvent &evt) {
    //updateMenu(evt.timeSinceLastFrame);
    return !_exit;
}

bool MenuState::frameEnded (const Ogre::FrameEvent &evt) {
    return !_exit;
}

void MenuState::keyPressed (const OIS::KeyEvent &e) {
    switch (e.key) {
        default:
            break;
    }
}

void MenuState::keyReleased (const OIS::KeyEvent &e) {
    switch (e.key) {
        case OIS::KC_ESCAPE:
            _exit = true;
            break;
        case OIS::KC_P:
            changeState(PlayState::getSingletonPtr());
            break;
        default:
            break;
    }
}

void MenuState::mouseMoved(const OIS::MouseEvent& e)
{
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMousePosition(e.state.X.abs, e.state.Y.abs);
}

void MenuState::mousePressed(const OIS::MouseEvent& e, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(convertMouseButton(id));
}

void MenuState::mouseReleased(const OIS::MouseEvent& e, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(convertMouseButton(id));
}

CEGUI::MouseButton MenuState::convertMouseButton(OIS::MouseButtonID id)
{
    CEGUI::MouseButton ceguiId;
    switch(id)
    {
        case OIS::MB_Left:
            ceguiId = CEGUI::LeftButton;
            break;
        case OIS::MB_Right:
            ceguiId = CEGUI::RightButton;
            break;
        case OIS::MB_Middle:
            ceguiId = CEGUI::MiddleButton;
            break;
        default:
            ceguiId = CEGUI::LeftButton;
    }
    return ceguiId;
}

void MenuState::createGUI()
{
    if(_sheet == NULL){

        _sheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow","Game/SheetMenu");

        //Start button
        CEGUI::Window* playButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button","Game/PlayButton");
        playButton->setText("PLAY");
        playButton->setSize(CEGUI::USize(CEGUI::UDim(0.2,0),CEGUI::UDim(0.05,0)));
        playButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.2,0),CEGUI::UDim(0.25,0)));
        playButton->subscribeEvent(CEGUI::PushButton::EventClicked,
        CEGUI::Event::Subscriber(&MenuState::play, this));

        //Instructions button
        CEGUI::Window* instructionsButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button","Game/InstructionsButton");
        instructionsButton->setText("INSTRUCTIONS");
        instructionsButton->setSize(CEGUI::USize(CEGUI::UDim(0.2,0),CEGUI::UDim(0.05,0)));
        instructionsButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.2,0),CEGUI::UDim(0.3,0)));
        instructionsButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MenuState::instructions, this));

        //Ranking button
        CEGUI::Window* rankingButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button","Game/RankingButton");
        rankingButton->setText("RANKING");
        rankingButton->setSize(CEGUI::USize(CEGUI::UDim(0.2,0),CEGUI::UDim(0.05,0)));
        rankingButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.2,0),CEGUI::UDim(0.35,0)));
        rankingButton->subscribeEvent(CEGUI::PushButton::EventClicked,CEGUI::Event::Subscriber(&MenuState::ranking, this));

        //Credits button
        CEGUI::Window* creditsButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button","Game/CreditsButton");
        creditsButton->setText("CREDITS");
        creditsButton->setSize(CEGUI::USize(CEGUI::UDim(0.2,0),CEGUI::UDim(0.05,0)));
        creditsButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.2,0),CEGUI::UDim(0.4,0)));
        creditsButton->subscribeEvent(CEGUI::PushButton::EventClicked,
        CEGUI::Event::Subscriber(&MenuState::credits, this));

        //Quit button
        CEGUI::Window* quitButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button","Game/QuitButton");
        quitButton->setText("EXIT");
        quitButton->setSize(CEGUI::USize(CEGUI::UDim(0.2,0),CEGUI::UDim(0.05,0)));
        quitButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.2,0),CEGUI::UDim(0.45,0)));
        quitButton->subscribeEvent(CEGUI::PushButton::EventClicked,
        CEGUI::Event::Subscriber(&MenuState::quit, this));

        //Attaching buttons
        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(playButton);
        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(rankingButton);
        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(instructionsButton);
        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(creditsButton);
        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(quitButton);
       _sheet->addChild(playButton);
        _sheet->addChild(rankingButton);
        _sheet->addChild(instructionsButton);
        _sheet->addChild(creditsButton);
        _sheet->addChild(quitButton);
        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(_sheet);

    }else{
        _sheet->show();
    }

}

bool MenuState::play(const CEGUI::EventArgs &e)
{
    changeState(PlayState::getSingletonPtr());

    return true;
}

bool MenuState::quit(const CEGUI::EventArgs &e)
{
    _exit = true;
    return true;
}

bool MenuState::instructions(const CEGUI::EventArgs &e)
{
    changeState(InstructionsState::getSingletonPtr());
    return true;
}

bool MenuState::ranking(const CEGUI::EventArgs &e)
{
    changeState(RankingState::getSingletonPtr());
    return true;
}

bool MenuState::credits(const CEGUI::EventArgs &e)
{
    changeState(CreditsState::getSingletonPtr());

    return true;
}

bool MenuState::hide(const CEGUI::EventArgs &e)
{
    _instructions->hide();
    return true;

}

void MenuState::menu(){

    _sceneMgr->getCamera("MainCamera")->setPosition(Ogre::Vector3(0, 0, 10));
    _sceneMgr->getCamera("MainCamera")->lookAt(Ogre::Vector3(0, 0, 8));

    Ogre::TexturePtr m_backgroundTexture = Ogre::TextureManager::getSingleton().createManual("BackgroundTexture",Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, Ogre::TEX_TYPE_2D,640, 480, 0, Ogre::PF_BYTE_BGR,Ogre::TU_DYNAMIC_WRITE_ONLY_DISCARDABLE);
    Ogre::Image m_backgroundImage;
    m_backgroundImage.load("background.jpg",Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
    m_backgroundTexture->loadImage(m_backgroundImage);
    Ogre::MaterialPtr material = Ogre::MaterialManager::getSingleton().create("BackgroundMaterial", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
    material->getTechnique(0)->getPass(0)->createTextureUnitState("BackgroundTexture");
    material->getTechnique(0)->getPass(0)->setLightingEnabled(false);
    _rect = new Ogre::Rectangle2D(true);
    _rect->setCorners(-1.0, 1.0, 1.0, -1.0);
    _rect->setMaterial("BackgroundMaterial");
    _rect->setRenderQueueGroup(Ogre::RENDER_QUEUE_BACKGROUND);
    _rect->setBoundingBox(Ogre::AxisAlignedBox(-100000.0*Ogre::Vector3::UNIT_SCALE, 100000.0*Ogre::Vector3::UNIT_SCALE));
    _backNode = _sceneMgr->getRootSceneNode()->createChildSceneNode("BackgroundMenu");
    _backNode->attachObject(_rect);

    _rectFog = new Ogre::Rectangle2D(true);
    _rectFog->setCorners(-1.0, 1.0, 1.0, -1.0);
    _rectFog->setBoundingBox(Ogre::AxisAlignedBox(-100000.0f * Ogre::Vector3::UNIT_SCALE, 100000.0f * Ogre::Vector3::UNIT_SCALE));
    Ogre::MaterialPtr materialFog = Ogre::MaterialManager::getSingleton().getByName("MaterialFog");
    materialFog->getTechnique(0)->getPass(0)->setDepthCheckEnabled(false);
    materialFog->getTechnique(0)->getPass(0)->setDepthWriteEnabled(false);
    materialFog->getTechnique(0)->getPass(0)->setLightingEnabled(false);
    _rectFog->setMaterial("MaterialFog");
    materialFog->getTechnique(0)->getPass(0)->getTextureUnitState(0)->setScrollAnimation(0.01, 0.01);
    _rectFog->setRenderQueueGroup(Ogre::RENDER_QUEUE_BACKGROUND);
    _rectFog->setBoundingBox(Ogre::AxisAlignedBox(-100000.0*Ogre::Vector3::UNIT_SCALE, 100000.0*Ogre::Vector3::UNIT_SCALE));
    _fogNode = _sceneMgr->getRootSceneNode()->createChildSceneNode("BackgroundFog");
    _fogNode->attachObject(_rectFog);

}
